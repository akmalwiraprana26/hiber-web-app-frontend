import { faEraser, faFilter, faPencilAlt, faPlus, faSearch, faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useRouter } from "next/dist/client/router";
import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";
import { SupplierClient, SupplierListViewModel } from "../../api/hiber-api";
import { RoleEnum } from "../../enums/enums";
import { PaginationValue } from "../../interface/IPagination";
import Authorize from "../shared/Authorize";
import LoadingPage from "../shared/components/LoadingPage";
import { ItemPerPagePicker } from "../shared/components/Pagination/ItemPerPagePicker";
import { Pagination } from "../shared/components/Pagination/Pagination";
import Layout from "../shared/Layout";

const SupplierListComponent: React.FunctionComponent<{}> = () => {
    const Router = useRouter();
    const [supplierName, setSupplierName] = useState<string>('');
    const [supplierData, setSupplierData] = useState<SupplierListViewModel[]>([]);
    const [isFilterOpened, setIsFilterOpened] = useState<boolean>(false);
    const [client, setClient] = useState<SupplierClient>();
    const [pagination, setPagination] = useState<PaginationValue>({
        currentPage: 1,
        itemPerPage: 5,
        totalData: 0,
    });
    const [isLoading, setIsLoading] = useState<boolean>(true);

    useEffect(() => {
        const fetchData = async () => {
            const client = new SupplierClient();

            resetData(client, pagination);
            setClient(client);
        }
        fetchData();
    }, []);
    const resetData = async (supplierClient: SupplierClient, paginate: PaginationValue) => {
        setIsLoading(true);
        try {
            const data = await supplierClient.getAll(supplierName, paginate.currentPage, paginate.itemPerPage);
            pagination.totalData = data.totalData;
            setPagination({ ...pagination });

            if (data.suppliers)
                setSupplierData(data.suppliers);
        } catch (error) {

        } finally {
            setIsLoading(false);
        }
    }

    const onClickFilter = async () => {
        if (!client) {
            return;
        }
        const paginate = pagination;
        paginate.currentPage = 1;
        resetData(client, paginate);
    }
    const onClickResetFilter = async () => {
        setSupplierName('');
        if (!client) {
            return;
        }
        setIsLoading(true);
        try {
            const data = await client.getAll(undefined, 1, pagination.itemPerPage);
            pagination.currentPage = 1;
            pagination.totalData = data.totalData;
            setPagination({ ...pagination });

            if (data.suppliers) {
                setSupplierData(data.suppliers);
            }
        } catch (error) {

        } finally {
            setIsLoading(false);
        }
    }

    const renderFilterForm = () => {
        if (isFilterOpened) {
            return (
                <div>
                    <div className='px-3'>
                        <div className='d-flex flex-row justify-content-between my-1'>
                            <p className='w-50 my-auto'>Nama Supplier</p>
                            <input type="text" value={supplierName} onChange={e => setSupplierName(e.target.value)} className='form-control w-50' placeholder='Search' />
                        </div>
                        <div className="d-flex flex-row justify-content-end">
                            <button className='btn btn-danger d-flex' onClick={() => onClickResetFilter()}><FontAwesomeIcon icon={faEraser} className='me-2 my-auto'></FontAwesomeIcon>Reset</button>
                            <button className='btn btn-primary ms-2 d-flex' onClick={() => onClickFilter()}><FontAwesomeIcon icon={faSearch} className='me-2 my-auto'></FontAwesomeIcon>Cari</button>
                        </div>
                    </div>
                </div>
            )
        }

        return undefined;

    }

    const onDeleteSupplier = async (supplierId: string) => {
        const response = await Swal.fire({
            icon: 'error',
            title: "Hapus Supplier?",
            html: '<span>Apakah anda yakin ingin menghapus supplier?</span> <br/> <br/><span class="text-danger">* Aksi ini tidak dapat di kembalikan</span>',
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak"
        })

        if (response.isConfirmed) {
            setIsLoading(true);
            try {
                const result = await client?.deleteSupplier(supplierId);

                if (result == true) {
                    Swal.fire({
                        title: "Sukses Mengapus Data",
                        text: "Data Sudah Disimpan",
                        allowOutsideClick: false,
                        confirmButtonText: 'OK'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            if (client)
                                resetData(client, pagination);
                        }
                    })
                }
            } catch (error) {

            } finally {
                setIsLoading(false);
            }
        }
    }

    const updatePaginate = async (itemPerPage: number, currentPage: number) => {
        const paginate = pagination;
        paginate.itemPerPage = itemPerPage;
        paginate.currentPage = currentPage;
        setPagination({ ...paginate });
        if (!client) {
            return;
        }
        resetData(client, paginate);
    }

    return (
        <div className='d-flex flex-column'>
            <LoadingPage loading={isLoading} />
            <h1>Daftar Supplier</h1>
            <hr />
            <div className='d-flex flex-column'>
                <div className='p-1 bg-light shadow-lg mb-2'>
                    <div className='d-flex flex-row justify-content-between'>
                        <p className='my-auto btn ms-auto fw-bold' onClick={() => setIsFilterOpened(!isFilterOpened)}><FontAwesomeIcon icon={faFilter} className='mx-2'></FontAwesomeIcon>Filter</p>
                    </div>
                    {renderFilterForm()}
                </div>

                <div className="d-flex flex-row">
                    <button className='btn btn-secondary ms-auto mb-2' onClick={() => { Router.push('/supplier/create') }}><FontAwesomeIcon icon={faPlus} className='me-2'></FontAwesomeIcon>Buat Supplier Baru</button>
                </div>

                {/* table data */}
                <div >
                    <table className="table table-striped table-hover">
                        <thead >
                            <tr>
                                <th scope="col" className="text-center">Nama Supplier</th>
                                <th scope="col" className="text-center">Email</th>
                                <th scope="col" className="text-center">Nomor Telepon</th>
                                <th scope="col" className="text-center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            {supplierData?.map(Q =>
                                <tr key={Q.supplierId}>
                                    <td className="pe-auto text-center">{Q.supplierName}</td>
                                    <td className="text-center">{Q.supplierEmail}</td>
                                    <td className="text-center">{Q.supplierPhone}</td>
                                    <td className="text-center">
                                        <button className='btn btn-danger me-2' onClick={() => onDeleteSupplier(Q.supplierId)}>
                                            <div className="text-light">
                                                <FontAwesomeIcon icon={faTrash} className='mx-1 text-light'></FontAwesomeIcon>Hapus
                                            </div>
                                        </button>
                                        <button className='btn btn-warning me-2' onClick={() => Router.push(`/supplier/edit/${Q.supplierId}`)}>
                                            <div className="text-light">
                                                <FontAwesomeIcon icon={faPencilAlt} className='mx-1 text-light'></FontAwesomeIcon>Ubah
                                            </div>
                                        </button>
                                    </td>
                                </tr>)}
                        </tbody>
                    </table>
                </div>
                <div className="d-flex flex-row">
                    <Pagination dataLength={pagination.totalData} dataPerPage={pagination.itemPerPage} currentPage={pagination.currentPage} onPageChange={e => updatePaginate(pagination.itemPerPage, e)}></Pagination>
                    <ItemPerPagePicker onChange={e => updatePaginate(e, 1)} />
                </div>
            </div>
        </div>
    )
}

export default function SupplierListPage() {
    return (
        <Authorize roleId={RoleEnum.ADMIN}>
            <Layout title="Supplier List">
                <SupplierListComponent></SupplierListComponent>
            </Layout>
        </Authorize>
    )
}