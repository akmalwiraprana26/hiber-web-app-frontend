import Joi from "joi";
import React, { useState } from "react";
import Swal from "sweetalert2";
import { SupplierClient, SupplierCreateEditModel } from "../../api/hiber-api";
import { RoleEnum } from "../../enums/enums";
import Authorize from "../shared/Authorize";
import BreadCrumbs from "../shared/components/BreadCrumbs";
import LoadingPage from "../shared/components/LoadingPage";
import Layout from "../shared/Layout";

interface IFormError {
    supplierEmail: {
        error: string
    },
    supplierName: {
        error: string
    },
    supplierPhone: {
        error: string
    },
}


const CreateSupplierComponent: React.FunctionComponent<{}> = () => {
    const [formValue, setFormValue] = useState<SupplierCreateEditModel>({
        supplierEmail: '',
        supplierName: '',
        supplierPhone: ''
    });

    const [formError, setFormError] = useState<IFormError>({
        supplierEmail: {
            error: ''
        },
        supplierName: {
            error: ''
        },
        supplierPhone: {
            error: ''
        },
    })

    const [isSubmitting, setIsSubmitting] = useState<boolean>(false);
    const [isChanged, setIsChanged] = useState<boolean>(false);

    const validateForm = (form, key?: string) => {
        const schema: {
            supplierName: Joi.SchemaLike,
            supplierEmail: Joi.SchemaLike,
            supplierPhone: Joi.SchemaLike
        } = {
            supplierEmail: '',
            supplierName: '',
            supplierPhone: ''
        }

        const errorMessages = {
            supplierEmail: '',
            supplierName: '',
            supplierPhone: ''
        }

        if (!key || key === 'supplierEmail') {
            schema['supplierEmail'] = Joi.string()
                .allow('')
                .email({ tlds: { allow: false } })
                .messages({
                    'string.email': 'Masukkan email yang valid'
                });
        }
        if (!key || key === 'supplierName') {
            schema['supplierName'] = Joi.string()
                .empty()
                .messages({
                    'string.empty': 'Nama Supplier harus diisi'
                });
        }
        if (!key || key === 'supplierPhone') {
            schema['supplierPhone'] = Joi.string()
                .allow('')
                .min(10)
                .messages({
                    'string.min': 'Nomor Handphone harus valid'
                });
        }

        const validationResult = Joi.object(schema).validate(form, {
            abortEarly: false
        });

        const err = validationResult.error;

        if (!err) {
            return undefined;
        }

        for (const detail of err.details) {
            const key = detail.path[0]?.toString() ?? '';
            errorMessages[key] = detail.message;
        }

        return errorMessages;
    }

    const resetForm = () => {
        setFormValue({
            supplierEmail: '',
            supplierName: '',
            supplierPhone: ''
        })

        setFormError({
            supplierEmail: {
                error: ''
            },
            supplierName: {
                error: ''
            },
            supplierPhone: {
                error: ''
            },
        })
    }

    const onSubmitForm = async (e: React.ChangeEvent<HTMLFormElement>) => {

        const client = new SupplierClient();

        e.preventDefault();
        const validationResult = validateForm(formValue);

        if (validationResult != undefined) {
            const createFormError: IFormError = formError;

            for (const key in createFormError) {
                createFormError[key].dirty = true;

                if (validationResult && validationResult[key]) {
                    createFormError[key].error = validationResult[key];
                }
                else {
                    createFormError[key].error = '';
                }
            }

            setFormError({
                ...createFormError
            });
        }
        else {
            if (client) {

                try {
                    setIsSubmitting(true);
                    const response = await client.createSupplier(formValue);
                    if (response) {
                        Swal.fire({
                            title: 'Berhasil memasukkan supplier baru',
                            text: 'Supplier baru berhasil ditambahkan',
                            icon: 'success'
                        });

                        setIsSubmitting(false);
                        resetForm();

                    }
                    else {
                        Swal.fire({
                            title: 'Gagal memasukkan supplier baru',
                            text: 'Sebuah kesalahan telah terjadi. Silakan coba lagi atau hubungi administrator',
                            icon: 'error'
                        });
                        setIsSubmitting(false);
                    }
                } catch (error) {
                    Swal.fire({
                        title: 'Gagal memasukkan supplier baru',
                        text: 'Sebuah kesalahan telah terjadi. Silakan coba lagi atau hubungi administrator',
                        icon: 'error'
                    });
                    setIsSubmitting(false);
                } finally {
                    setIsSubmitting(false);
                }
            }
        }
    }

    const onFormChanged = (key: string, value: string) => {
        setIsChanged(true);
        const newFormValue: SupplierCreateEditModel = formValue;
        newFormValue[key] = value;

        const createSupplierFormError: IFormError = formError;

        const validationResult = validateForm(formValue, key);

        
        if (validationResult && validationResult[key]) {
            createSupplierFormError[key].error = validationResult[key];
        }
        else{
            createSupplierFormError[key].error = ''
        }

        setFormValue({
            ...newFormValue
        });

        setFormError({
            ...createSupplierFormError
        })
    }

    return (
        <div>
            <BreadCrumbs href="/supplier" isChanged={isChanged} text="Kembali ke list supplier"/>
            <LoadingPage loading={isSubmitting}/>
            <h3>Buat Supplier Baru</h3>
            <hr />

            <form onSubmit={onSubmitForm} >
                <fieldset disabled={isSubmitting}>

                    <div className='form-group mb-3'>
                        <label htmlFor="inputNameProduct" className="form-label fw-bold">Nama Supplier<span className='text-danger'>*</span></label>
                        <input type="text" className="form-control" id="inputNameProduct" value={formValue.supplierName} onChange={(e) => onFormChanged('supplierName', e.target.value)} />
                        <span className="text-danger small">{formError.supplierName.error}</span>
                    </div>
                    <div className='form-group mb-3'>
                        <label htmlFor="inputNameProduct" className="form-label fw-bold">Email Supplier</label>
                        <input type="text" className="form-control" id="inputNameProduct" value={formValue.supplierEmail} onChange={(e) => onFormChanged('supplierEmail', e.target.value)} />
                        <span className="text-danger small">{formError.supplierEmail.error}</span>
                    </div>
                    <div className='form-group mb-3'>
                        <label htmlFor="inputNameProduct" className="form-label fw-bold">Nomor Telepon Supplier</label>
                        <input type="text" className="form-control" id="inputNameProduct" value={formValue.supplierPhone} onChange={(e) => onFormChanged('supplierPhone', e.target.value)} />
                        <span className="text-danger small">{formError.supplierPhone.error}</span>
                    </div>

                    <input type="submit" className='btn btn-primary d-flex ms-auto' value='Buat'/>
                </fieldset>
            </form>
        </div>
    )
}



export default function CreateSupplierPage() {
    return (
        <Authorize roleId={RoleEnum.ADMIN}>
            <Layout title="Buat Supplier">
                <CreateSupplierComponent></CreateSupplierComponent>
            </Layout>
        </Authorize>
    )
}