import React, { FormEvent, useEffect, useState } from "react";
import { LoginFormModel, AuthClient } from "../../api/hiber-api";
import { PasswordInput } from "../user/create";
import Image from "next/image"
import { useCookies } from "react-cookie";
import { useRouter } from "next/dist/client/router";

const Login: React.FunctionComponent<{}> = (props) => {
    const [form, setForm] = useState<LoginFormModel>({
        password: '',
        username: '',
    });
    const [isLoginBtnDisabled, setIsBtnLoginDisabled] = useState<boolean>(true);
    const [isRememberMeChecked, setIsRememberMeChecked] = useState<boolean>(false);
    const [errorMsg, setErrorMsg] = useState<string>('');
    const [cookies, setCookie] = useCookies(['username', 'userId', 'roleId', 'roleName']);
    const router = useRouter();
    useEffect(() => {
        validateForm();
    }, [form]);
    const tryLogin = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        setIsBtnLoginDisabled(true);
        setErrorMsg('');
        const client = new AuthClient();
        try {
            const response = await client.tryLogin({
                username: form.username,
                password: form.password
            });
            if(response == null) {
                setErrorMsg('Username atau password salah!');
            }
            else {
                let options = { path: '/'}

                if(isRememberMeChecked) {
                    let expiredDate: Date = new Date();
                    expiredDate.setHours(new Date().getHours() + 3);
                    options['expires'] = expiredDate;
                }
                else {
                    options['expires'] = 0;
                }

                setCookie("username", response.username, options);
                setCookie("userId", response.userId, options);
                setCookie("roleId", response.roleId, options);
                setCookie("roleName", response.roleName, options);

                router.push('/dashboard');
            }
        } catch (error) {
            setErrorMsg('Terjadi kesalahan sistem, mohon coba kembali');
        } finally {
            setIsBtnLoginDisabled(false);
        }
    }

    const validateForm = () => {
        if(form.username.trim() == '') {
            setIsBtnLoginDisabled(true);
        }
        else if(form.password.trim() == '') {
            setIsBtnLoginDisabled(true);
        }
        else {
            setIsBtnLoginDisabled(false);
        }
    }

    const onPasswordChange = (value: string) => {
        const newForm = form;
        newForm.password = value;
        setForm({
            ...newForm
        });
    }
    
    const onUsernameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = e.target.value;
        const newForm = form;
        newForm.username = newValue;
        setForm({
            ...newForm
        });
    }

    const onRememberMeChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = e.target.checked;
        setIsRememberMeChecked(newValue); 
    }

    return (
        <div className='login-container bg-light'>
            <div className='w-50 d-flex flex-column bg-light shadow-lg'>
                <h3 className='fw-bold mx-auto mt-auto'>Selamat Datang Kembali,</h3>
                <br />
                <h6 className='mx-auto mb-auto w-75 text-center'>Silahkan mengisi form di samping untuk dapat mengakses website</h6>
            </div>
            <div className='bg-light text-end p-2 shadow-lg'>
                <Image src='/assets/HI_Ber-removebg-preview.png' width='76' height='50' alt='temp'/>
            </div>
            <div className='w-50 d-flex flex-column text-center'>
                <form onSubmit={tryLogin} className='w-75 m-auto p-5'>
                    <div className="mb-3">
                        <input type="text" className="form-control shadow" value={form.username} onChange={onUsernameChange} id="usernameInput" placeholder='Username'/>
                    </div>
                    <div className="mb-3">
                        <PasswordInput onChange={onPasswordChange} value={form.password} className='shadow' placeholder='Password'/>
                    </div>
                    <div className="mb-3 form-check text-start">
                        <input type="checkbox" className="form-check-input" id="rememberMeInput" onChange={onRememberMeChange}/>
                        <label className="form-check-label" htmlFor="rememberMeInput">Remember me?</label>
                    </div>
                    <br />
                    <button type="submit" className="btn login-btn shadow w-100" disabled={isLoginBtnDisabled}>Login</button>
                    <span className='text-danger'>{errorMsg}</span>
                </form>
            </div>
        </div>
    )
}

export default Login;