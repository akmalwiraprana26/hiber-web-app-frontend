import Layout from "../../shared/Layout";
import React, { useEffect, useState } from "react";
import { DropdownModel, ProductClient } from "../../../api/hiber-api";
import Select from "react-select";
import { GetServerSideProps } from "next";
import Swal from "sweetalert2";
import Joi from "joi";
import { RoleEnum } from "../../../enums/enums";
import Authorize from "../../shared/Authorize";
import LoadingPage from "../../shared/components/LoadingPage";
import PriceInput, { ReturnType } from "../../shared/components/PriceInput";
import BreadCrumbs from "../../shared/components/BreadCrumbs";
import { useRouter } from "next/dist/client/router";

interface IEditForm {
    productName: string,
    productPrice: number,
    productBasePrice: number,
    supplier: string,
    productStock: number,
    productCategory: string,
    minimumStockLimit: number,
}

interface IProductEditFormError {
    productName: {
        error: string
    },
    productPrice: {
        error: string
    },
    productBasePrice: {
        error: string
    },
    supplier: {
        error: string
    },
    productStock: {
        error: string
    },
    productCategory: {
        error: string
    },
    minimumStockLimit: {
        error: string
    },
}

export const EditProductForm: React.FunctionComponent<{ id }> = (props) => {

    const [formValue, setFormValue] = useState<IEditForm>({
        productBasePrice: 0,
        productCategory: '',
        productName: '',
        productPrice: 0,
        productStock: 0,
        minimumStockLimit: 1,
        supplier: ''
    });

    const [formError, setFormError] = useState<IProductEditFormError>({
        productBasePrice: {
            error: ''
        },
        productCategory: {
            error: ''
        },
        productName: {
            error: ''
        },
        productPrice: {
            error: ''
        },
        productStock: {
            error: ''
        },
        supplier: {
            error: ''
        },
        minimumStockLimit: {
            error: ''
        }
    })
    const Router = useRouter();
    const [isChanged, setIsChanged] = useState<boolean>(false);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [client, setClient] = useState<ProductClient>();
    const [supplierList, setSupplierList] = useState<DropdownModel[]>([{
        label: '',
        value: ''
    }]);
    const [categoriesList, setCategoriesList] = useState<DropdownModel[]>([{
        label: '',
        value: ''
    }]);
    const [imagePreviewUrl, setImagePreviewUrl] = useState<any>("/assets/placeholder-image.png")
    const [photoForm, setPhotoForm] = useState<any>(null);

    useEffect(() => {
        const initializeClient = async () => {
            const productClient = new ProductClient();
            initializeData(productClient);
            setClient(productClient);
        }
        initializeClient();
        return;
    }, [])

    const initializeData = async (productClient: ProductClient) => {
        setIsLoading(true)
        if (!productClient) {
            return;
        }
        const data = await productClient.getFormData()

        if (data.supplierList) {
            setSupplierList(data.supplierList);
        }

        if (data.categoriesList) {
            setCategoriesList(data.categoriesList);
        }

        const productData = await productClient.getProductDetail(props.id);

        const currentData: IEditForm = {
            productBasePrice: productData.productBasePrice,
            productCategory: productData.productCategoryId.toString(),
            productName: productData.productName ?? "",
            productPrice: productData.productPrice,
            productStock: productData.productStock,
            minimumStockLimit: productData.minimumStockLimit,
            supplier: productData.supplierId
        }

        if (productData.imageName != null) {
            setImagePreviewUrl(productData.imageSrc);
        }


        setFormValue(currentData);

        setIsLoading(false)


    }

    const validateForm = (form, key?: string) => {
        const schema: {
            productBasePrice: Joi.SchemaLike,
            productCategory: Joi.SchemaLike,
            productName: Joi.SchemaLike,
            productPrice: Joi.SchemaLike,
            productStock: Joi.SchemaLike,
            supplier: Joi.SchemaLike
        } = {
            productBasePrice: '',
            productCategory: '',
            productName: '',
            productPrice: '',
            productStock: '',
            supplier: ''
        }

        const errorMessages = {
            productBasePrice: '',
            productCategory: '',
            productName: '',
            productPrice: '',
            productStock: '',
            supplier: ''
        }

        if (!key || key === 'productName') {
            schema['productName'] = Joi.string()
                .empty()
                .messages({
                    'string.empty': 'Nama Produk harus diisi'
                });
        }

        if (!key || key === 'productCategory') {
            schema['productCategory'] = Joi.string()
                .empty()
                .messages({
                    'string.empty': 'Kategori Produk harus diisi'
                });
        }

        if (!key || key === 'productBasePrice') {
            schema['productBasePrice'] = Joi.number()
                .min(1)
                .messages({
                    'number.min': 'Harga Modal Produk harus diisi'
                });
        }

        if (!key || key === 'productPrice') {
            schema['productPrice'] = Joi.number()
                .min(1)
                .messages({
                    'number.min': 'Harga Produk harus diisi'
                });
        }

        if (!key || key === 'productStock') {
            schema['productStock'] = Joi.number()
                .min(1)
                .messages({
                    'number.min': 'Stok Produk harus lebih dari 1'
                });
        }

        if (!key || key === 'supplier') {
            schema['supplier'] = Joi.string()
                .empty()
                .messages({
                    'string.empty': 'Supplier harus diisi'
                });
        }

        if (!key || key === 'minimumStockLimit') {
            schema['minimumStockLimit'] = Joi.number()
                .min(1)
                .messages({
                    'number.min': 'Minimum Stok Produk harus lebih dari 0'
                });
        }

        const validationResult = Joi.object(schema).validate(form, {
            abortEarly: false
        });

        const err = validationResult.error;

        if (!err) {
            return undefined;
        }

        for (const detail of err.details) {
            const key = detail.path[0]?.toString() ?? '';
            errorMessages[key] = detail.message;
        }

        return errorMessages;
    }



    const onFormChanged = (key: string, value: string) => {
        setIsChanged(true)
        const newFormValue: IEditForm = formValue;
        newFormValue[key] = value;

        const editProductFormError: IProductEditFormError = formError;

        const validationResult = validateForm(formValue, key);

        if (validationResult && validationResult[key]) {
            editProductFormError[key].error = validationResult[key];
        }
        else {
            editProductFormError[key].error = ''
        }

        setFormValue({
            ...newFormValue
        });

        setFormError({
            ...editProductFormError
        })
    }

    const onFormSubmit = async (e: React.ChangeEvent<HTMLFormElement>) => {

        e.preventDefault();

        const validationResult = validateForm(formValue);

        if (validationResult != undefined) {
            const createFormError: IProductEditFormError = formError;

            for (const key in createFormError) {
                createFormError[key].dirty = true;

                if (validationResult && validationResult[key]) {
                    createFormError[key].error = validationResult[key];
                }
                else {
                    createFormError[key].error = '';
                }
            }

            setFormError({
                ...createFormError
            });
        }
        else {


            if (client) {
                setIsLoading(true);
                try {
                    let response;
                    if (photoForm != null) {
                        response = await client.editProduct(formValue.productName, formValue.productStock, formValue.minimumStockLimit, formValue.productPrice, formValue.productBasePrice, parseInt(formValue.productCategory), formValue.supplier, photoForm.name, {
                            data: photoForm,
                            fileName: photoForm.name
                        }, props.id);
                    } else {
                        response = await client.editProduct(formValue.productName, formValue.productStock, formValue.minimumStockLimit, formValue.productPrice, formValue.productBasePrice, parseInt(formValue.productCategory), formValue.supplier, null, null, props.id);
                    }

                    if (response) {
                        Swal.fire({
                            title: 'Berhasil mengubah barang',
                            text: 'Barang baru berhasil diubah',
                            icon: 'success'
                        });

                        Router.push('/product')
                    }
                    else {
                        Swal.fire({
                            title: 'Gagal mengubah barang',
                            text: 'Sebuah kesalahan telah terjadi. Silakan coba lagi atau hubungi administrator',
                            icon: 'error'
                        });
                    }
                } catch (error) {
                    Swal.fire({
                        title: 'Gagal mengubah barang',
                        text: 'Sebuah kesalahan telah terjadi. Silakan coba lagi atau hubungi administrators',
                        icon: 'error'
                    });
                } finally {
                    setIsLoading(false);
                }

            }
        }

    }

    const onChangePicture = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.files && e.target.files[0]) {
            setIsChanged(true)
            let imageFile = e.target.files[0]

            const reader = new FileReader();
            reader.onload = x => {
                setPhotoForm(imageFile)
                setImagePreviewUrl(x.target?.result)
            }
            reader.readAsDataURL(imageFile);
        }

    }

    return (
        <div className='p-3'>
            <LoadingPage loading={isLoading} />
            <BreadCrumbs href="/product" isChanged={isChanged} text="Kembali ke Daftar produk" />
            <h3 className="mb-4">Ubah Produk</h3>
            <hr />
            <form onSubmit={onFormSubmit}>
                <fieldset disabled={isLoading}>
                    <div className='form-group mb-3'>
                        <div className="d-flex flex-row">
                            <div style={{ width: 400, height: 300, objectFit: 'contain' }}>
                                <img src={imagePreviewUrl} className="img-thumbnail" style={{ maxWidth: '100%', maxHeight: "100%" }} />
                            </div>
                            {(photoForm !== null || imagePreviewUrl != '/assets/placeholder-image.png' ) && <div className="btn btn-danger text-white align-self-center ms-4" onClick={() => {
                                setPhotoForm(null)
                                setImagePreviewUrl("/assets/placeholder-image.png")
                            }}>Hapus Foto</div>}
                            
                        </div>

                        <label htmlFor="inputProductPhoto" className="form-label fw-bold">Foto Barang</label>
                        <input style={{color:'transparent'}} accept="image/*" type="file" className="form-control" id="inputProductPhoto" onChange={(e) => onChangePicture(e)} 
                        />
                    </div>
                    <div className='form-group mb-3'>
                        <label htmlFor="inputNameProduct" className="form-label fw-bold">Nama Barang<span className='text-danger'>*</span></label>
                        <input type="text" className="form-control" id="inputNameProduct" value={formValue.productName} onChange={(e) => onFormChanged('productName', e.target.value)} />
                        <span className="text-danger small">{formError.productName.error}</span>
                    </div>

                    <div className='form-group mb-3 d-flex'>
                        <div className="col-md-4">
                            <label htmlFor="inputBasePrice" className="form-label fw-bold">Modal<span className='text-danger'>*</span></label>
                            
                            <PriceInput className="form-control" value={formValue.productBasePrice} onChange={(e) => onFormChanged('productBasePrice', e)} returnType={ReturnType.NUMBER} />
                            <span className="text-danger small">{formError.productBasePrice.error}</span>

                        </div>
                        <div className="col-md-4"></div>
                        <div className="col-md-4">
                            <label htmlFor="inputProductPrice" className="form-label fw-bold">Harga Barang<span className='text-danger'>*</span></label>
                            
                            <PriceInput className="form-control" value={formValue.productPrice} onChange={(e) => onFormChanged('productPrice', e)} returnType={ReturnType.NUMBER} />
                            <span className="text-danger small">{formError.productPrice.error}</span>
                        </div>
                    </div>

                    <div className='form-group mb-3 d-flex'>
                        <div className="col-md-4">
                            <label htmlFor="inputProductStock" className="form-label fw-bold">Jumlah Barang<span className='text-danger'>*</span></label>
                            <input type="number" className="form-control" id="inputProductStock" value={formValue.productStock} onChange={(e) => onFormChanged('productStock', e.target.value)} />
                            <span className="text-danger small">{formError.productStock.error}</span>
                            <br></br>
                            <label className="form-label fw-bold">Supplier<span className='text-danger'>*</span></label>
                            <Select
                                isSearchable={true}
                                options={supplierList}
                                value={{
                                    label: supplierList.find(Q => Q.value === formValue.supplier)?.label ?? "",
                                    value: formValue.supplier
                                }}
                                onChange={(e) => onFormChanged('supplier', e.value)}
                            />
                            <span className="text-danger small">{formError.supplier.error}</span>
                        </div>
                        <div className="col-md-4"></div>
                        <div className="col-md-4">
                            <label htmlFor="inputProductCategory" className="form-label fw-bold">Jenis Barang<span className='text-danger'>*</span></label>

                            <Select
                                isSearchable={true}
                                options={categoriesList}
                                value={{
                                    label: categoriesList.find(Q => Q.value === formValue.productCategory)?.label ?? "",
                                    value: formValue.productCategory
                                }}
                                onChange={(e) => onFormChanged('productCategory', e.value)}
                            />
                            <span className="text-danger small">{formError.productCategory.error}</span>
                            <br></br>
                            <label className="form-label fw-bold">Minimum Stok Limit<span className='text-danger'>*</span></label>
                            <input type="number" className="form-control" id="mnputMinimumStockLimitStock" value={formValue.minimumStockLimit} onChange={(e) => onFormChanged('minimumStockLimit', e.target.value)} />
                            <span className="text-danger small">{formError.minimumStockLimit.error}</span>
                        </div>
                    </div>

                    <div className="d-flex">
                        <input className='btn btn-primary ms-auto mb-4' type="submit" value={"Submit"}></input>
                    </div>

                </fieldset>
            </form>
        </div>
    )
}

const EditProductPage: React.FunctionComponent<{ id: string }> = (props) => {
    return (
        <Authorize roleId={RoleEnum.ADMIN}>
            <Layout title="Edit Product">
                <EditProductForm id={props.id}></EditProductForm>
            </Layout>
        </Authorize>
    )
}

export default EditProductPage;

export const getServerSideProps: GetServerSideProps<{ id: string }> = async (context) => {
    if (context.params) {
        const id = context.params['id'];

        if (typeof id === 'string') {
            return {
                props: {
                    id: id
                }
            }
        }
    }

    return {
        props: {
            id: ''
        }
    };
}