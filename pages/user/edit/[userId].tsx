import Joi from "joi";
import { GetServerSideProps } from "next";
import { useRouter } from "next/dist/client/router";
import React, { useEffect, useState } from "react";
import { useCookies } from "react-cookie";
import Swal from "sweetalert2";
import {  UserClient, UserUpdateModel, UserUpdateResponseModel } from "../../../api/hiber-api";
import Authorize from "../../shared/Authorize";
import BreadCrumbs from "../../shared/components/BreadCrumbs";
import LoadingPage from "../../shared/components/LoadingPage";
import Layout from "../../shared/Layout";
import { PasswordInput } from "../create";

interface UpdateFormError {
    email: {
        error: string
    };
    name: {
        error: string
    };
    password: {
        error: string
    };
    username: {
        error: string
    };
}

const UserDetail: React.FunctionComponent<{userId: string}> = (props) => {
    const [updateForm, setUpdateForm] = useState<UserUpdateModel>(
        {
            updatedUserId: props.userId,
            username: '',
            name: '',
            email: '',
            password: '',
            isAdmin: false
        }
    )

    const [formError, setFormError] = useState<UpdateFormError>({
        username: {
            error: ''
        },
        name: {
            error: ''
        },
        email: {
            error: ''
        },
        password: {
            error: ''
        },
    });

    const [isSubmitting, setIsSubmitting] = useState<boolean>(false);
    const [isChanged, setIsChanged] = useState<boolean>(false);
    const [cookies, setCookie, removeCookie] = useCookies(['username', 'userId', 'roleId', 'roleName']);
    const router = useRouter();
    
    useEffect(() => {
        fetchUserData();
    }, []);

    const fetchUserData = async () => {
        setIsSubmitting(true);
        const client = new UserClient();
        try {
            const data = await client.getUser(props.userId);
            setUpdateForm({
                ...updateForm,
                email: data.email,
                isAdmin: data.roleId == 1,
                name: data.name,
                username: data.username
            });
        } catch (error) {
            
        } finally {
            setIsSubmitting(false);
        }
    }

    const onFormChange = (key: string, value: string) => {
        setIsChanged(true);

        const newFormValue: UserUpdateModel = updateForm;
        newFormValue[key] = value

        const newRegisterFormError: UpdateFormError = formError;

        const formToValidate = {
            username: updateForm.username,
            name: updateForm.name,
            email: updateForm.email,
            password: updateForm.password
        }

        const validationResult = validateForm(formToValidate, key);
        
        if (validationResult && validationResult[key]) {
            newRegisterFormError[key].error = validationResult[key];
        }
        else{
            newRegisterFormError[key].error = ''
        }
        setUpdateForm({
            ...newFormValue
        });
        setFormError({
            ...newRegisterFormError
        });
    }

    const onRoleChange = (value: boolean) => {
        setIsChanged(true);

        const newFormValue: UserUpdateModel = updateForm;
        newFormValue.isAdmin = value;
        setUpdateForm({
            ...newFormValue
        });
    }

    const validateForm = (form, key?: string) => {
        const schema: {
            username: Joi.SchemaLike,
            name: Joi.SchemaLike,
            email: Joi.SchemaLike,
            password: Joi.SchemaLike,
        } = {
            username: '',
            name: '',
            email: '',
            password: '',
        }

        const errorMessages = {
            username: '',
            name: '',
            email: '',
            password: '',
        };

        if (!key || key === 'username') {
            schema['username'] = Joi.string()
                .empty()
                .messages({
                    'string.empty': 'Username harus diisi',
                });
        }
        
        if (!key || key === 'name') {
            schema['name'] = Joi.string()
                .empty()
                .messages({
                    'string.empty': 'Nama harus diisi',
                });
        }
        
        if (!key || key === 'email') {
            schema['email'] = Joi.string()
                .empty()
                .email({ tlds: { allow: false } })
                .messages({
                    'string.empty': 'Email harus diisi',
                    'string.email': 'Email tidak valid',
                });
        }
        
        if (!key || key === 'password') {
            schema['password'] = Joi.string()
                .empty()
                .messages({
                    'string.empty': 'Password harus diisi',
                });
        }

        const validationResult = Joi.object(schema).validate(form, {
            abortEarly: false
        });

        const err = validationResult.error;
        
        if (!err){
            return undefined;
        }

        for (const detail of err.details){
            const key = detail.path[0]?.toString() ?? '';
            errorMessages[key] = detail.message;
        }

        return errorMessages;
    }

    const setNewCookie = () => {
        if(props.userId == cookies.userId) {
            setCookie("roleId", updateForm.isAdmin ? 1 : 2);
            setCookie("username", updateForm.username);
        }

        router.push('/dashboard');
    }

    const onFormSubmit = async (e: React.ChangeEvent<HTMLFormElement>) => {
        e.preventDefault();
        const formToValidate = {
            username: updateForm.username,
            name: updateForm.name,
            email: updateForm.email,
            password: updateForm.password
        }
        const validationResult = validateForm(formToValidate);
        
        if(validationResult != undefined) {
            const newRegisterFormError: UpdateFormError = formError;

            for (const key in newRegisterFormError) {
                newRegisterFormError[key].dirty = true;
    
                if (validationResult && validationResult[key]) {
                    newRegisterFormError[key].error = validationResult[key];
                }
                else{
                    newRegisterFormError[key].error = '';
                }
            }
            
            setFormError({
                ...newRegisterFormError
            });
        }
        else {
            setIsSubmitting(true);
            try {
                const client = new UserClient();
                const response: UserUpdateResponseModel = await client.updateUser(updateForm, cookies.userId);
                
                if(response.isSuccess == false) {
                    Swal.fire({
                        title: response.errorMsg,
                        text: 'Gagal mengubah data user',
                        icon: 'error'
                    });
                }
                else if(response.isSuccess == true) {
                    Swal.fire({
                        title: response.errorMsg,
                        text: 'User berhasil diubah',
                        icon: 'success'
                    });
                    setNewCookie();
                } else {
                    Swal.fire({
                        title: response.errorMsg,
                        text: 'Sebuah kesalahan telah terjadi. Silakan coba lagi atau hubungi administrator',
                        icon: 'error'
                    });
                }
            } catch (error) {
                Swal.fire({
                    title: 'Gagal mengedit info user',
                    text: 'Sebuah kesalahan telah terjadi. Silakan coba lagi atau hubungi administrator',
                    icon: 'error'
                });
            } finally {
                setIsSubmitting(false);
            }
        }
    }
    
    return (
        <div>
            <LoadingPage loading={isSubmitting}/>
            {cookies.userId != props.userId ? 
                <BreadCrumbs href="/user" isChanged={isChanged} text="Kembali ke list user"/>
                    :   <BreadCrumbs href="/dashboard" isChanged={isChanged} text="Kembali ke dashboard"/>}
            <h3>Edit akun</h3>            
            <hr />
            <form onSubmit={onFormSubmit}>
                <fieldset disabled={isSubmitting}>
                    <div className='form-group mb-3'>
                        <label htmlFor="inputName" className="form-label fw-bold">Name</label>
                        <input type="text" className="form-control" id="inputName" value={updateForm.name} onChange={(e) => onFormChange('name', e.target.value)} disabled={cookies.userId != props.userId}/>
                        <span className="text-danger small">{formError.name.error}</span>
                    </div>
                    <div className='form-group mb-3'>
                        <label htmlFor="inputUsername" className="form-label fw-bold">Username</label>
                        <input type="text" className="form-control" id="inputUsername" value={updateForm.username} onChange={(e) => onFormChange('username', e.target.value)} disabled={cookies.userId != props.userId}/>
                        <span className="text-danger small">{formError.username.error}</span>
                    </div>
                    <div className='form-group mb-3'>
                        <label htmlFor="inputEmail" className="form-label fw-bold">Email</label>
                        <input type="email" className="form-control" id="inputEmail" value={updateForm.email} onChange={(e) => onFormChange('email', e.target.value)} disabled={cookies.userId != props.userId}/>
                        <span className="text-danger small">{formError.email.error}</span>
                    </div>
                    {cookies.roleId == 1 ? 
                        <div className='form-check mb-3'>
                            <input type="checkbox" className="form-check-input" id="inputRole" checked={updateForm.isAdmin} onChange={(e) => onRoleChange(e.target.checked)}/>
                            <label className="form-check-label" htmlFor="inputRole">is Admin</label>
                        </div> : undefined}
                    <div className='form-group mb-3'>
                        <label htmlFor="inputPassword" className="form-label fw-bold">Password Saat Ini</label>
                        <PasswordInput value={updateForm.password ?? ''} onChange={(value) => onFormChange('password', value)}/>
                        <span className="text-danger small">{formError.password.error}</span>
                    </div>
                    <br />
                    <input type="submit" className='btn btn-primary d-flex ms-auto' value='Simpan'/>
                </fieldset>
            </form>
        </div>
    );
}

const UserDetailPage: React.FunctionComponent<{userId: string}> = (props) => {
    return (
        <Authorize onlyLoggedIn={true}>
            <Layout title='User Detail'>
                <UserDetail userId={props.userId}></UserDetail>
            </Layout>
        </Authorize>
    );
}

export const getServerSideProps: GetServerSideProps<{ userId: string }> = async (context) => {
    if (context.params) {
        const userId = context.params['userId'];

        if (typeof userId === 'string') {
            return {
                props: {
                    userId: userId
                }
            }
        }
    }

    return {
        props: {
            userId: ''
        }
    };
}

export default UserDetailPage;