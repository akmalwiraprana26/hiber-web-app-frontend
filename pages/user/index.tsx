import { faEraser, faFilter, faInfoCircle, faPlus, faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { useCookies } from "react-cookie";
import { UserClient, UserViewModel, DropdownIntModel, UserPaginationModel, RoleClient } from "../../api/hiber-api";
import { RoleEnum } from "../../enums/enums";
import { PaginationValue } from "../../interface/IPagination";
import Authorize from "../shared/Authorize";
import { ItemPerPagePicker } from "../shared/components/Pagination/ItemPerPagePicker";
import { Pagination } from "../shared/components/Pagination/Pagination";
import Layout from "../shared/Layout";
import Select from 'react-select';
import LoadingPage from "../shared/components/LoadingPage";

const UserGridData: React.FunctionComponent<{
    users: UserViewModel[],
    userId: string
}> = (props) => {
    const renderRowData = () => {
        return props.users.map(Q => {
            return (
                <tr key={Q.userId}>
                    <td>
                        <p>{Q.name}</p>
                    </td>
                    <td>
                        <p>{Q.username}</p>
                    </td>
                    <td>
                        <p>{Q.email}</p>
                    </td>
                    <td>
                        <p>{Q.roleId == 1 ? 'Admin' : 'Karyawan'}</p>
                    </td>
                    <td>
                        <Link href={'/user/edit/' + Q.userId}>
                            <a className='btn btn-primary'><FontAwesomeIcon icon={faInfoCircle} className='mx-1 text-light'></FontAwesomeIcon>Detail</a>
                        </Link>
                    </td>
                </tr>
            )
        });
    }

    return (
        <div >
            <table className='table table-striped m-0 text-center table-hover'>
                <thead className='table-header'>
                    <tr>
                        <th>Nama</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {renderRowData()}
                </tbody>
            </table>
        </div>
    );
}

interface UserFilterForm {
    name: string;
    username: string;
    email: string;
    role: DropdownIntModel;
}

const User: React.FunctionComponent<{}> = (props) => {
    const [listUser, setListUser] = useState<UserViewModel[]>([]);
    const [listRole, setListRole] = useState<DropdownIntModel[]>([]);
    const [cookies, setCookie, removeCookie] = useCookies(['username', 'userId', 'roleId', 'roleName']);
    const [filterForm, setFilterForm] = useState<UserFilterForm>({
        email: '',
        name: '',
        role: {
            label: '',
            value: undefined
        },
        username: ''
    });
    const [isFilterOpened, setIsFilterOpened] = useState<boolean>(false);
    const [pagination, setPagination] = useState<PaginationValue>({
        currentPage: 1,
        itemPerPage: 5,
        totalData: 0,
    });
    const [isLoading, setIsLoading] = useState<boolean>(true);

    useEffect(() => {
        getFilterData();
        fetchData(pagination, filterForm);
    }, []);

    const fetchData = async (pagination: PaginationValue, filter: UserFilterForm) => {
        setIsLoading(true);

        const client = new UserClient();
        try {
            const data: UserPaginationModel = await client.getAllUser(filter.name, filter.username, filter.email, cookies.userId, filter.role.value as number, pagination.currentPage, pagination.itemPerPage);
            setPagination({...pagination, totalData: data.totalData});
            setListUser(data.users as UserViewModel[]);
        } catch (error) {
            
        } finally {
            setIsLoading(false);
        }
    }

    const getFilterData = async () => {
        setIsLoading(true);
        try {
            const client = new RoleClient();
            const data: DropdownIntModel[] = await client.getAllRole();
    
            setListRole(data as DropdownIntModel[]);
        } catch (error) {
            
        } finally {
            setIsLoading(false);
        }
    }

    const resetData = () => {
        const newPagination = {
            ...pagination,
            currentPage: 1
        };
        const newFilter = {
            email: '',
            name: '',
            role: {
                label: '',
                value: undefined
            },
            username: ''
        }

        setFilterForm({...newFilter});
        setPagination({...newPagination});

        try {
            fetchData(newPagination, newFilter);
        } catch (error) {
            
        }
    }

    const updatePaginate = async (itemPerPage: number, currentPage: number) => {
        const paginate = pagination;
        paginate.itemPerPage = itemPerPage;
        paginate.currentPage = currentPage;
        setPagination(paginate);

        fetchData(paginate, filterForm);
    }

    const searchFilteredData = () => {
        const newPagination = {
            ...pagination,
            currentPage: 1
        };

        setPagination({...newPagination});
        try {
            fetchData(newPagination, filterForm);
        } catch (error) {
            
        }
    }

    const renderFilterForm = () => {
        if (isFilterOpened) {
            return (
                <div>
                    <div className='px-3'>
                        <div className='d-flex flex-row justify-content-between my-1'>
                            <p className='w-50 my-auto'>Nama</p>
                            <input type="text" value={filterForm.name} className='form-control w-50' placeholder='Nama User' onChange={e => setFilterForm({...filterForm, name: e.target.value})}/>
                        </div>
                        <div className='d-flex flex-row justify-content-between my-1'>
                            <p className='w-50 my-auto'>Username</p>
                            <input type="text" value={filterForm.username} className='form-control w-50' placeholder='Username User' onChange={e => setFilterForm({...filterForm, username: e.target.value})}/>
                        </div>
                        <div className='d-flex flex-row justify-content-between my-1'>
                            <p className='w-50 my-auto'>Email</p>
                            <input type="text" value={filterForm.email} className='form-control w-50' placeholder='Email User' onChange={e => setFilterForm({...filterForm, email: e.target.value})}/>
                        </div>
                        <div className='d-flex flex-row justify-content-between mb-1'>
                            <p className='w-50 my-auto'>Role</p>
                            <div className='w-50'>
                                <Select options={listRole} onChange={selected => setFilterForm({...filterForm, role: selected})} value={filterForm.role}/>
                            </div>
                        </div>
                        <div className="d-flex flex-row justify-content-end">
                            <button className='btn btn-danger d-flex' onClick={resetData}><FontAwesomeIcon icon={faEraser} className='me-2 my-auto'></FontAwesomeIcon>Reset</button>
                            <button className='btn btn-primary ms-2 d-flex' onClick={searchFilteredData}><FontAwesomeIcon icon={faSearch} className='me-2 my-auto'></FontAwesomeIcon>Cari</button>
                        </div>
                    </div>
                </div>
            )
        }

        return undefined;
    }

    return (
        <div className='d-flex flex-column'>
            <LoadingPage loading={isLoading}/>
            <h1>Daftar User</h1>
            <hr />
            <div className='p-1 bg-light shadow-lg'>
                <div className='d-flex flex-row justify-content-between'>
                    <p className='my-auto btn ms-auto fw-bold' onClick={() => setIsFilterOpened(!isFilterOpened)}><FontAwesomeIcon icon={faFilter} className='mx-2'></FontAwesomeIcon>Filter</p>
                </div>
                {renderFilterForm()}
            </div>

            <Link href={'/user/create'}>
                <a className='btn btn-secondary my-2 ms-auto'><FontAwesomeIcon icon={faPlus} className='me-2'></FontAwesomeIcon>Tambah Akun</a>
            </Link>
            <UserGridData users={listUser} userId={cookies.userId}></UserGridData>

            <div className="d-flex flex-row">
                <Pagination dataLength={pagination.totalData} dataPerPage={pagination.itemPerPage} currentPage={pagination.currentPage} onPageChange={e => updatePaginate(pagination.itemPerPage, e)}></Pagination>
                <ItemPerPagePicker onChange={e => updatePaginate(e, 1)} />
            </div>
        </div>
    );
}

export default function UserPage() {
    return (
        <Authorize roleId={RoleEnum.ADMIN}>
            <Layout title='User'>
                <User></User>
            </Layout>
        </Authorize>
    );
}