import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEraser, faFilter, faInfoCircle, faSearch } from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { PaymentTypeClient, PaymentTypeViewModel, TransactionClient, TransactionPaginationModel, TransactionViewModel } from "../../api/hiber-api";
import TransactionStatus from "../shared/components/transaction/transactionStatus";
import Layout from "../shared/Layout";
import Select from 'react-select';
import { DateRangePicker, FocusedInputShape } from 'react-dates';
import { Moment } from "moment";
import ReturStatus from "../shared/components/retur/returStatus";
import Authorize from "../shared/Authorize";
import { RoleEnum } from "../../enums/enums";
import { PaginationValue } from "../../interface/IPagination";
import { ItemPerPagePicker } from "../shared/components/Pagination/ItemPerPagePicker";
import { Pagination } from "../shared/components/Pagination/Pagination";
import { CopyClipboard } from "../shared/components/CopyClipboard";
import LoadingPage from "../shared/components/LoadingPage";
import PriceFormat from "../shared/components/PriceFormat";

export interface DropdownModel {
    label: string;
    value: number | undefined;
}

export interface FilterModel {
    transactionId: string;
    buyerName: string;
    paymentType: DropdownModel;
    isPaid: DropdownModel;
    dateFrom: Moment | null;
    dateTo: Moment | null;
}

const TransactionGridData: React.FunctionComponent<{
    transactions: TransactionViewModel[]
}> = (props) => {
    const renderRowData = () => {
        return props.transactions.map(Q =>
            <tr key={Q.transactionId}>
                <td >
                    <p>{Q.transactionId} <span>
                        <CopyClipboard text={Q.transactionId} />
                    </span></p>

                </td>
                <td>
                    <p>{Q.buyerName}</p>
                </td>
                <td>
                    <p>{Q.paymentTypeName}</p>
                </td>
                <td>
                    <p><PriceFormat value={Q.totalPrice}/></p>
                </td>
                <td>
                    <TransactionStatus isPaid={Q.isPaid} />
                    {Q.isInReturProcess ? <ReturStatus className='mt-1' /> : undefined}
                </td>
                <td>
                    <Link href={'/transaction/' + Q.transactionId}>
                        <a className='btn btn-primary'> <FontAwesomeIcon icon={faInfoCircle} className='mx-1 text-light'></FontAwesomeIcon>Detail</a>
                    </Link>
                </td>
            </tr>);
    }

    return (
        <div>
            <table className='table table-striped m-0 text-center table-hover'>
                <thead className='table-header'>
                    <tr>
                        <th>ID Transaksi</th>
                        <th>Nama Pembeli</th>
                        <th>Metode Pembayaran</th>
                        <th>Total Harga</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {renderRowData()}
                </tbody>
            </table>
        </div>
    );
}

const Transaction: React.FunctionComponent<{}> = (props) => {
    const [listTransaction, setListTransaction] = useState<TransactionViewModel[]>([]);
    const [listPaymentType, setListPaymentType] = useState<DropdownModel[]>([]);
    const [listStatus, setListStatus] = useState<DropdownModel[]>([]);
    const [focused, setFocused] = useState<FocusedInputShape | null>(null);
    const [filter, setFilter] = useState<FilterModel>({
        transactionId: '',
        buyerName: '',
        dateFrom: null,
        dateTo: null,
        isPaid: {
            value: undefined,
            label: ''
        },
        paymentType: {
            value: undefined,
            label: ''
        }
    });
    const [isFilterShowed, setIsFilterShowed] = useState<boolean>(false);
    const [pagination, setPagination] = useState<PaginationValue>({
        currentPage: 1,
        itemPerPage: 5,
        totalData: 0,
    });
    const [isLoading, setIsLoading] = useState<boolean>(true);

    useEffect(() => {
        getFilterData();
        fetchData(pagination, filter);
    }, []);

    const getFilterData = async () => {
        setIsLoading(true);
        const paymentTypeClient = new PaymentTypeClient();

        try {
            const response: PaymentTypeViewModel[] = await paymentTypeClient.getAllPaymentType();
            response.forEach(Q => {
                listPaymentType.push({
                    value: Q.paymentTypeId,
                    label: Q.paymentTypeName ?? ''
                });
            })
            setListPaymentType(listPaymentType as DropdownModel[]);

            let listStatus: {}[] = [];
            listStatus.push({
                value: 1,
                label: 'Lunas'
            });
            listStatus.push({
                value: 0,
                label: 'Belum lunas'
            });
            setListStatus(listStatus as DropdownModel[]);
        } catch (error) {

        } finally {
            setIsLoading(false);
        }
    }

    const fetchData = async (pagination: PaginationValue, filterData: FilterModel) => {
        setIsLoading(true);
        const transactionClient = new TransactionClient();
        try {
            const data: TransactionPaginationModel = await transactionClient.getAllTransaction(
                filterData.dateFrom == null ? undefined : filterData.dateFrom.toISOString(),
                filterData.dateTo == null ? undefined : filterData.dateTo.toISOString(),
                filterData.paymentType.value == 0 ? undefined : filterData.paymentType.value,
                filterData.isPaid.value == undefined ? undefined : filterData.isPaid.value == 1 ? true : false,
                filterData.buyerName,
                filterData.transactionId,
                pagination.currentPage,
                pagination.itemPerPage);

            setPagination({ ...pagination, totalData: data.totalData })
            setListTransaction(data.transactions as TransactionViewModel[]);
        } catch (error) {

        } finally {
            setIsLoading(false);
        }
    }

    const changeFilterState = () => {
        setIsFilterShowed(!isFilterShowed);
    }

    const onTransactionIdChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        const newFilter = filter;
        newFilter.transactionId = value;
        setFilter({
            ...newFilter
        });
    }

    const onBuyerNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        const newFilter = filter;
        newFilter.buyerName = value;
        setFilter({
            ...newFilter
        });
    }

    const onPaymentTypeChange = (selected) => {
        const newFilter = filter;
        newFilter.paymentType = selected;
        setFilter({
            ...newFilter
        });
    }

    const onStatusChange = (selected) => {
        const newFilter = filter;
        newFilter.isPaid = selected;
        setFilter({
            ...newFilter
        });
    }

    const searchFilteredData = async () => {
        const newPagination = {
            ...pagination,
            currentPage: 1
        };

        setPagination({ ...newPagination });
        try {
            fetchData(newPagination, filter);
        } catch (error) {

        }
    }

    const resetFilter = () => {
        const newPagination = {
            ...pagination,
            currentPage: 1
        };
        const newFilter = {
            transactionId: '',
            buyerName: '',
            dateFrom: null,
            dateTo: null,
            isPaid: {
                value: undefined,
                label: ''
            },
            paymentType: {
                value: undefined,
                label: ''
            }
        };

        setFilter({ ...newFilter });
        setPagination({ ...newPagination });

        try {
            fetchData(newPagination, newFilter);
        } catch (error) {

        }
    }

    const updatePaginate = async (itemPerPage: number, currentPage: number) => {
        const paginate = pagination;
        paginate.itemPerPage = itemPerPage;
        paginate.currentPage = currentPage;
        setPagination(paginate);

        fetchData(paginate, filter);
    }

    const renderFilterForm = () => {
        if (isFilterShowed) {
            return (
                <div>
                    <div className='px-3'>
                        <div className='d-flex flex-row justify-content-between my-1'>
                            <p className='w-50 my-auto'>ID Transaksi</p>

                            <input type="text" value={filter.transactionId} className='form-control w-50' placeholder='Search' onChange={onTransactionIdChange} />
                        </div>
                        <div className='d-flex flex-row justify-content-between my-1'>
                            <p className='w-50 my-auto'>Nama Pembeli</p>
                            <input type="text" value={filter.buyerName} className='form-control w-50' placeholder='Search' onChange={onBuyerNameChange} />
                        </div>
                        <div className='d-flex flex-row justify-content-between mb-1'>
                            <p className='w-50 my-auto'>Metode Pembayaran</p>
                            <div className='w-50'>
                                <Select options={listPaymentType} onChange={onPaymentTypeChange} value={filter.paymentType} />
                            </div>
                        </div>
                        <div className='d-flex flex-row justify-content-between mb-1'>
                            <p className='w-50 my-auto'>Status</p>
                            <div className='w-50'>
                                <Select options={listStatus} onChange={onStatusChange} value={filter.isPaid} />
                            </div>
                        </div>
                        <div className='d-flex flex-row justify-content-between mb-1'>
                            <p className='w-50 my-auto'>Tanggal Transaksi</p>

                            <div className='w-50'>
                                <DateRangePicker
                                    small={true}
                                    block={true}
                                    isOutsideRange={() => false}
                                    startDatePlaceholderText={'Tanggal awal'}
                                    endDatePlaceholderText={'Tanggal akhir'}
                                    showClearDates={true}
                                    startDate={filter.dateFrom}
                                    startDateId="your_unique_start_date_id"
                                    endDate={filter.dateTo}
                                    endDateId="your_unique_end_date_id"
                                    onDatesChange={({ startDate, endDate }) => setFilter({ ...filter, dateFrom: startDate, dateTo: endDate })}
                                    focusedInput={focused}
                                    onFocusChange={(focusedInput) => setFocused(focusedInput)}
                                />
                            </div>
                        </div>
                        <div className="d-flex flex-row justify-content-end">
                            <button className='btn btn-danger d-flex' onClick={resetFilter}><FontAwesomeIcon icon={faEraser} className='me-2 my-auto'></FontAwesomeIcon>Reset</button>
                            <button className='btn btn-primary ms-2 d-flex' onClick={searchFilteredData}><FontAwesomeIcon icon={faSearch} className='me-2 my-auto'></FontAwesomeIcon>Cari</button>
                        </div>
                    </div>
                </div>
            )
        }

        return undefined;
    }

    return (
        <div>
            <LoadingPage loading={isLoading} />
            <h1>Daftar Transaksi</h1>
            <hr />
            <div className='p-1 bg-light shadow-lg'>
                <div className='d-flex flex-row justify-content-between'>
                    <p className='my-auto btn ms-auto fw-bold' onClick={changeFilterState}><FontAwesomeIcon icon={faFilter} className='mx-2'></FontAwesomeIcon>Filter</p>
                </div>
                {renderFilterForm()}
            </div>
            <br />
            <TransactionGridData transactions={listTransaction}></TransactionGridData>

            <div className="d-flex flex-row">
                <Pagination dataLength={pagination.totalData} dataPerPage={pagination.itemPerPage} currentPage={pagination.currentPage} onPageChange={e => updatePaginate(pagination.itemPerPage, e)}></Pagination>
                <ItemPerPagePicker onChange={e => updatePaginate(e, 1)} />
            </div>
        </div>
    );
}

export default function TransactionPage() {
    return (
        <Authorize roleId={RoleEnum.ADMIN}>
            <Layout title='Transaction'>
                <Transaction></Transaction>
            </Layout>
        </Authorize>
    );
}