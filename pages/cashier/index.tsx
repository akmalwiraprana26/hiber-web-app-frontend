import React, { useEffect, useState } from "react"
import Layout from "../shared/Layout"
import Select from 'react-select';
import { DropdownModel } from "../transaction";
import { CashierClient, PaymentTypeClient, PaymentTypeViewModel, PdfClient, UserClient, UserViewModel } from "../../api/hiber-api";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { DriverDropdownModel } from "../shared/components/retur/returTransactionModal";
import { SingleDatePicker } from 'react-dates';
import { Moment } from "moment";
import 'moment/locale/id';
import moment from "moment";
import Swal from "sweetalert2";
import Authorize from "../shared/Authorize";
import { PaymentTypeEnum } from "../../enums/enums";
import LoadingPage from "../shared/components/LoadingPage";
import PriceInput, { ReturnType } from "../shared/components/PriceInput";
import PriceFormat from "../shared/components/PriceFormat";
import { useCookies } from "react-cookie";

var idLocale = require('moment/locale/id');
moment.updateLocale('id', idLocale);

const CREDIT_PAYMENT_TYPE = 1;

interface CartForm {
    listProduct: CartProductModel[];
    isCreateDelivery: number;
    buyerName: string;
    address?: string | undefined;
    paymentType: DropdownModel;
    driver?: DriverDropdownModel | undefined;
    deadlineDate: Moment | null;
    interest?: string | undefined;
    totalPrice: number;
    buyerMoney: string;
    buyerChange: number;
}

interface CartProductModel {
    productId: string;
    barcodeId: string;
    productName?: string | undefined;
    productQuantity: number;
    productPrice: number;
    productUnitName?: string | undefined;
    productTotalPrice: number;
    productAvailableStock: number;
    isValid: boolean;
}

const InputBarcodeOrName: React.FunctionComponent<{
    onGetValidInput: (input: string) => void
}> = (props) => {
    const [value, setValue] = useState<string>('');
    const [isInputValid, setIsInputValid] = useState<boolean>(false);

    useEffect(() => {
        validateInput(value);
    }, [value]);

    const validateInput = (input: string) => {
        let isValid = true;

        //check alphanumeric spasi
        if (!input.match(/^[A-z0-9 ]+$/i)) {
            isValid = false;
        }

        setIsInputValid(isValid);
    }

    const onInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = e.target.value;

        setValue(newValue);
    }

    const onInputKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
        const pressed = e.keyCode;
        if (pressed == 13 && isInputValid) {
            props.onGetValidInput(value);

            setIsInputValid(false);
            setValue('');
        }
    }

    const getInputClassName = () => {
        if (isInputValid) return 'is-valid';

        return 'is-invalid';
    }

    return (
        <div>
            <input type="text" className={'form-control input-barcode ' + getInputClassName()} onChange={onInputChange} onKeyDown={onInputKeyDown} value={value} autoFocus />
            <small className='text-secondary'>Silahkan scan barcode produk atau ketik manual kode barcode</small>
        </div>
    )
}

const CartProductRowData: React.FunctionComponent<{
    product: CartProductModel,
    onRemoveProductClick: (productId: string) => void,
    onChange: (newQty: string, isNewQtyValid: boolean, productId: string) => void
}> = (props) => {
    const [errorMessage, setErrorMessage] = useState<string>('');
    const [ttlPrice, setTtlPrice] = useState<number>(0);

    useEffect(() => {
        let isInputValid = validate(props.product.productQuantity.toString());
        if (isInputValid) {
            if (props.product.productQuantity.toString().trim() != '') {
                setTtlPrice(parseInt(props.product.productQuantity.toString()) * props.product.productPrice);
            }
            else {
                setTtlPrice(0);
            }
        }
        else {
            setTtlPrice(0);
        }
    }, [props.product.productQuantity])

    const validate = (inputValue: string) => {
        if (inputValue.trim() == '') {
            setErrorMessage('Input tidak boleh kosong');
            return false;
        }

        let inputQty = parseInt(inputValue);

        if (inputQty > props.product.productAvailableStock) {
            setErrorMessage('Input lebih besar dari pada stok');
            return false;
        }
        else if (inputQty <= 0) {
            setErrorMessage('Input harus minimal 1');
            return false;
        }

        setErrorMessage('');
        return true;
    }

    const onInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = e.target.value;
        let isInputValid = validate(newValue);
        if (isInputValid) {
            if (newValue.trim() != '') {
                setTtlPrice(parseInt(newValue) * props.product.productPrice);
            }
            else {
                setTtlPrice(0);
            }
        }
        else {
            setTtlPrice(0);
        }

        props.onChange(newValue, isInputValid, props.product.productId);
    }

    const onRemoveButtonClick = () => {
        props.onRemoveProductClick(props.product.productId);
    }

    return (
        <tr key={props.product.productId}>
            <td>
                <p>{props.product.productName}</p>
            </td>
            <td>
                <div className="d-flex flex-row justify-content-center">
                    <input type="number" className="me-2 w-25" onChange={onInputChange} value={props.product.productQuantity == 0 ? '' : props.product.productQuantity} />
                    <p className="my-auto col-2 text-start">{props.product.productUnitName}</p>
                </div>
                <small className='text-danger'>{errorMessage}</small>
            </td>
            <td>
                <p>{props.product.productAvailableStock + " " + props.product.productUnitName}</p>
            </td>
            <td>
                <p><PriceFormat value={props.product.productPrice} /></p>
            </td>
            <td>
                <p><PriceFormat value={ttlPrice} /></p>
            </td>
            <td>
                <a className='btn shadow shadow-lg bg-dark text-light' onClick={onRemoveButtonClick}><FontAwesomeIcon icon={faTrash} className='m-auto' /></a>
            </td>
        </tr>
    );
}

const CartProductGridData: React.FunctionComponent<{
    listProduct: CartProductModel[],
    onRemoveProductClick: (productId: string) => void,
    onChange: (newQty: string, isNewQtyValid: boolean, productId: string) => void
}> = (props) => {
    const renderRowData = () => {
        const list: CartProductModel[] = props.listProduct as CartProductModel[];
        if (list.length > 0) {
            return list.map(Q => {
                return <CartProductRowData onRemoveProductClick={props.onRemoveProductClick} onChange={props.onChange} key={Q.productId} product={Q}></CartProductRowData>
            });
        }
        else {
            return (
                <tr>
                    <td colSpan={6}>No Data</td>
                </tr>
            )
        }
    }

    return (
        <div>
            <table className='table table-striped m-0 text-center'>
                <thead className='table-header'>
                    <tr>
                        <th>Nama Barang</th>
                        <th>Jumlah Barang</th>
                        <th>Stok saat ini</th>
                        <th>Harga Barang</th>
                        <th>Total Harga</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {renderRowData()}
                </tbody>
            </table>
        </div>
    );
}

const CashierModal: React.FunctionComponent<{
    form: CartForm,
    onSuccessPaid: () => void
    setShowLoading: (isShow: boolean) => void,
}> = (props) => {
    const [isPayButtonDisabled, setIsPayButtonDisabled] = useState<boolean>(false)
    const renderProductList = () => {
        if (props.form.listProduct && props.form.listProduct.length > 0) {
            const list = props.form.listProduct.map(Q => {
                return (
                    <tr key={Q.productId} className='d-flex'>
                        <p className='col-6 m-0'>{Q.productName}</p>
                        <p className='col-3 m-0'>{Q.productQuantity + " " + Q.productUnitName}</p>
                        <p className='col-3 m-0'><PriceFormat value={Q.productTotalPrice}></PriceFormat></p>
                    </tr>
                );
            });
            return list;
        }

        return undefined;
    }

    const renderDriverConfirmation = () => {
        if (props.form.isCreateDelivery) {
            return (
                <div>
                    <div className='d-flex flex-row justify-content-between m-0'>
                        <p className='fw-bold'>Pengiriman</p>
                    </div>
                    <table className='table table-striped border border-2'>
                        <tbody>
                            <tr className='d-flex'>
                                <p className='col-6 m-0'>Nama Pengirim</p>
                                <p className='col-6 m-0'>{props.form.driver?.label}</p>
                            </tr>
                            <tr className='d-flex'>
                                <p className='col-6 m-0'>Alamat Pengiriman</p>
                                <p className='col-6 m-0'>{props.form.address}</p>
                            </tr>
                        </tbody>
                    </table>
                </div>
            );
        }

        return undefined;
    }

    const renderPaymentConfirmation = () => {
        return (
            <div>
                <div className='d-flex flex-row justify-content-between m-0'>
                    <p className='fw-bold'>Pembayaran</p>
                </div>
                <table className='table table-striped border border-2'>
                    <tbody>
                        <tr className='d-flex'>
                            <p className='col-6 m-0'>Metode Pembayaran</p>
                            <p className='col-6 m-0'>{props.form.paymentType.label}</p>
                        </tr>
                        {props.form.paymentType.value == CREDIT_PAYMENT_TYPE ?
                            <tr className='d-flex'>
                                <p className='col-6 m-0'>Bunga per pembayaran</p>
                                <p className='col-6 m-0'>{props.form.interest + "%"}</p>
                            </tr>
                            : undefined
                        }
                        {props.form.paymentType.value == CREDIT_PAYMENT_TYPE ?
                            <tr className='d-flex'>
                                <p className='col-6 m-0'>Batas waktu pembayaran</p>
                                <p className='col-6 m-0'>{props.form.deadlineDate?.format('dddd, DD MMMM YYYY')}</p>
                            </tr>
                            : undefined
                        }
                    </tbody>
                </table>
            </div>
        );
    }

    const base64ToArrayBuffer = (base64: string) => {
        var binary_string = window.atob(base64);
        var len = binary_string.length;
        var bytes = new Uint8Array(len);
        for (var i = 0; i < len; i++) {
            bytes[i] = binary_string.charCodeAt(i);
        }
        return bytes.buffer;
    }

    const printInstallment = async (transactionId: string, pdfClient: PdfClient) => {
        const resultInstallment = await pdfClient.generateInstallmentApplication(transactionId);

        var blobInstallment = new Blob([base64ToArrayBuffer(resultInstallment)], { type: 'application/pdf' });
        var blobInstallmentURL = URL.createObjectURL(blobInstallment);
        window.open(blobInstallmentURL);
    }

    const printReceipt = async (transactionId: string, pdfClient: PdfClient) => {
        const result = await pdfClient.generateReceipt(transactionId);

        var blob = new Blob([base64ToArrayBuffer(result)], { type: 'application/pdf' });
        var blobURL = URL.createObjectURL(blob);
        window.open(blobURL);
    }

    const onPaidClick = async () => {
        setIsPayButtonDisabled(true);
        props.setShowLoading(true);
        const client = new CashierClient();
        const pdfClient = new PdfClient();
        try {
            const response = await client.saveCashierData({
                isCreateDelivery: props.form.isCreateDelivery == 1 ? true : false,
                paymentTypeId: props.form.paymentType.value ?? -1,
                totalPrice: props.form.totalPrice,
                address: props.form.address,
                buyerName: props.form.buyerName,
                deadlineDate: props.form.deadlineDate?.toISOString(),
                driverId: props.form.driver?.value,
                interest: parseInt(props.form.interest ?? '-1'),
                listProduct: props.form.listProduct.map(Q => {
                    return {
                        productId: Q.productId,
                        productQuantity: Q.productQuantity,
                        productTotalPrice: Q.productTotalPrice
                    }
                })
            });

            const isPrintReceipt = await Swal.fire({
                title: 'Berhasil menyimpan transaksi',
                icon: 'success',
                html: '<span>Apakah anda ingin cetak struk?</span>',
                showCancelButton: true,
                focusConfirm: false,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak"
            })

            if (response != null && props.form.paymentType.value == PaymentTypeEnum.KREDIT) {
                printInstallment(response, pdfClient);
            }

            if (isPrintReceipt.isConfirmed) {
                printReceipt(response, pdfClient);
            }

            props.onSuccessPaid();
        } catch (error) {
            Swal.fire({
                title: 'Gagal melakukan transaksi',
                text: 'Transaksi gagal',
                icon: 'error'
            });
        } finally {
            setIsPayButtonDisabled(false);
            props.setShowLoading(false);
        }
    }

    return (
        <div className="modal fade" id="cashierStaticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex={-1} aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div className="modal-dialog modal-lg">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="staticBackdropLabel">Apakah anda yakin data ingin di simpan?</h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">
                        <div className='d-flex flex-row justify-content-between m-0'>
                            <p className='fw-bold'>Data pembeli</p>
                        </div>
                        <table className='table table-striped border border-2'>
                            <tbody>
                                <tr className='d-flex'>
                                    <p className='col-6 m-0'>Nama Pembeli</p>
                                    <p className='col-6 m-0'>{props.form.buyerName}</p>
                                </tr>
                            </tbody>
                        </table>
                        <div className='d-flex flex-row justify-content-between m-0'>
                            <p className='fw-bold'>Daftar Barang</p>
                        </div>
                        <table className='table table-striped border border-2'>
                            <tbody>
                                {renderProductList()}
                            </tbody>
                        </table>
                        {renderDriverConfirmation()}
                        {renderPaymentConfirmation()}
                        <div className='d-flex ms-auto justify-content-between w-50'>
                            <p className='text-start fw-bold'>Total pembayaran</p>
                            <p className='text-end fw-bold'><PriceFormat value={props.form.totalPrice}></PriceFormat></p>
                        </div>
                        <div className='d-flex ms-auto justify-content-between w-50'>
                            <p className='text-start fw-bold'>Kembalian</p>
                            <p className='text-end fw-bold'><PriceFormat value={props.form.buyerChange}></PriceFormat></p>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                        <button type="button" className="btn btn-primary" onClick={onPaidClick} disabled={isPayButtonDisabled} data-bs-dismiss="modal">Bayar</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

const Cashier: React.FunctionComponent<{}> = (props) => {
    const [listPaymentType, setListPaymentType] = useState<DropdownModel[]>([]);
    const [listDriver, setListDriver] = useState<DriverDropdownModel[]>([]);
    const [addressErrMsg, setAddressErrMsg] = useState<string>('');
    const [interestErrMsg, setInterestErrMsg] = useState<string>('');
    const [deadlineDateErrMsg, setDeadlineDateErrMsg] = useState<string>('');
    const [buyerNameErrMsg, setBuyerNameErrMsg] = useState<string>('');
    const [buyerMoneyErrMsg, setBuyerMoneyErrMsg] = useState<string>('');
    const [focused, setFocused] = useState(false);
    const [cookies, setCookie, removeCookie] = useCookies(['cashier']);
    const [cartForm, setCartForm] = useState<CartForm>({
        isCreateDelivery: 0,
        paymentType: {
            label: '',
            value: -1
        },
        totalPrice: 0,
        buyerMoney: '0',
        buyerChange: 0,
        address: undefined,
        buyerName: '',
        deadlineDate: null,
        driver: {
            label: '',
            value: undefined
        },
        interest: undefined,
        listProduct: []
    });
    const [isPayButtonDisabled, setIsPayButtonDisabled] = useState<boolean>(true);
    const [isLoading, setIsLoading] = useState<boolean>(true);

    useEffect(() => {
        fetchData();
    }, []);

    useEffect(() => {
        validatePayButton();
        saveDataToCookie();
    }, [cartForm]);

    const setSavedData = () => {
        const savedData = cookies.cashier;
        if(savedData != null) {
            let newCartForm: CartForm = {
                ...savedData,
                deadlineDate: savedData.deadlineDate == null ? null : moment(savedData.deadlineDate)
            };

            setCartForm({...newCartForm});
        }
    }

    const saveDataToCookie = () => {
        let options = { path: '/'};
        options['expires'] = 0;
        setCookie("cashier", cartForm, options);
    }

    const fetchData = async () => {
        setIsLoading(true);
        const paymentTypeClient = new PaymentTypeClient();
        const userClient = new UserClient();
        try {
            const response: PaymentTypeViewModel[] = await paymentTypeClient.getAllPaymentType();
            let listPayment: DropdownModel[] = [];
            response.forEach(Q => {
                listPayment.push({
                    value: Q.paymentTypeId,
                    label: Q.paymentTypeName as string
                });
            })
            setListPaymentType(listPayment as DropdownModel[]);

            const newForm = cartForm;

            if (listPayment.length > 0) {
                newForm.paymentType = {
                    label: listPayment[listPayment.length - 1]?.label as string,
                    value: listPayment[listPayment.length - 1]?.value
                }
            }

            const responseDriver: UserViewModel[] = await userClient.getAllDriver();
            let listDriver: DriverDropdownModel[] = [];
            responseDriver.forEach(Q => {
                listDriver.push({
                    value: Q.userId,
                    label: Q.name as string
                });
            })
            setListDriver(listDriver as DriverDropdownModel[]);

            if (listDriver.length > 0) {
                newForm.driver = {
                    label: listDriver[0]?.label as string,
                    value: listDriver[0]?.value
                }
            }

            setCartForm({ ...newForm });
        } catch (error) {

        } finally {
            setIsLoading(false);
            setSavedData();
        }
    }

    const isProductAlreadyExist = (input: string) => {
        var product = cartForm.listProduct.filter(Q => (Q.barcodeId === input || Q.productName?.toLowerCase() === input.trim().toLocaleLowerCase()));

        if (product.length > 0) {
            return true;
        }

        return false;
    }

    const onGetValidInput = async (input: string) => {
        //get product
        if (isProductAlreadyExist(input) == false) {
            setIsLoading(true);
            const client = new CashierClient();

            try {
                const product = await client.getProductByBarcodeIdOrName(input);
                if (product) {
                    const newListProduct = cartForm.listProduct;
                    newListProduct.push({
                        productId: product.productId,
                        productPrice: product.productPrice,
                        productQuantity: 1,
                        productTotalPrice: 1 * product.productPrice,
                        productName: product.productName,
                        productUnitName: product.productUnitName,
                        productAvailableStock: product.productStock,
                        barcodeId: product.barcodeId as string,
                        isValid: true
                    });

                    const newCartForm = cartForm;
                    cartForm.totalPrice = calculatePrice(newListProduct);
                    setCartForm({
                        ...newCartForm,
                        listProduct: newListProduct
                    })
                } else {
                    Swal.fire({
                        title: 'Barang tidak ditemukan',
                        text: 'Barcode atau nama yang dimasukan salah',
                        icon: 'error'
                    });
                }

            } catch (error) {

            } finally {
                setIsLoading(false);
            }
        }
        else {
            const newListProduct = cartForm.listProduct.map(Q => {
                if (Q.barcodeId === input || Q.productName?.toLowerCase() === input.trim().toLowerCase()) {
                    let newQty = Q.productQuantity == Q.productAvailableStock ? Q.productQuantity : (Q.productQuantity + 1);
                    return {
                        ...Q,
                        productQuantity: newQty,
                        productTotalPrice: newQty * Q.productPrice
                    } as CartProductModel
                }
                else {
                    return Q;
                }
            })
            const newCartForm = cartForm;
            cartForm.totalPrice = calculatePrice(newListProduct);
            setCartForm({
                ...newCartForm,
                listProduct: newListProduct
            })
        }
    }

    const onCreateDeliveryChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setAddressErrMsg('');

        const newValue = parseInt(e.target.value);
        const newForm = cartForm;
        newForm.isCreateDelivery = newValue;

        setCartForm({ ...newForm });
    }

    const onPaymentTypeChange = (selected) => {
        setInterestErrMsg('');
        setDeadlineDateErrMsg('');
        setBuyerMoneyErrMsg('');

        const newForm = cartForm;
        newForm.paymentType = selected;
        newForm.buyerChange = 0;
        newForm.buyerMoney = '0';

        setCartForm({ ...newForm });
    }

    const onRemoveProductClick = (productId: string) => {
        let newList: CartProductModel[] = [];
        newList = cartForm.listProduct.filter(Q => Q.productId !== productId);

        setCartForm({ ...cartForm, listProduct: newList, totalPrice: calculatePrice(newList) });
    }

    const calculatePrice = (newList: CartProductModel[]) => {
        if (newList.length > 0) {
            return newList.reduce((sum, Q) => {
                return sum + (Q.productQuantity * Q.productPrice);
            }, 0);
        }

        return 0;
    }

    const onChange = (newQty: string, isNewQtyValid: boolean, productId: string) => {
        let newList: CartProductModel[] = [];
        var newQuantity = 0;
        if (!isNewQtyValid && newQty != '') {
            newQuantity = parseInt(newQty);
        }
        else if (isNewQtyValid) {
            newQuantity = parseInt(newQty);
        }
        newList = cartForm.listProduct.map(Q => {
            if (Q.productId == productId) {
                return {
                    ...Q,
                    productQuantity: newQuantity,
                    productTotalPrice: newQuantity * Q.productPrice,
                    isValid: isNewQtyValid
                }
            }

            return Q;
        });
        const newForm = cartForm;
        newForm.listProduct = newList;
        newForm.totalPrice = calculatePrice(newList);
        newForm.buyerChange = calculateChange();

        setCartForm({ ...newForm });
    }

    const validatePayButton = () => {
        let isBtnDisabled = false;
        if (cartForm.buyerName.trim() == '' || cartForm.buyerName == undefined) {
            isBtnDisabled = true;
        }
        else if (cartForm.isCreateDelivery && (cartForm.address == '' || cartForm.address == undefined)) {
            isBtnDisabled = true;
        }
        else if (cartForm.paymentType.value == PaymentTypeEnum.KREDIT && (cartForm.deadlineDate == null || cartForm.interest == null || cartForm.interest.trim() == '')) {
            isBtnDisabled = true;
        }
        else if (cartForm.listProduct && cartForm.listProduct.length == 0) {
            isBtnDisabled = true;
        }
        else if (cartForm.paymentType.value == PaymentTypeEnum.TUNAI && (cartForm.buyerMoney == undefined || cartForm.buyerMoney.trim() == '' || parseInt(cartForm.buyerMoney) < cartForm.totalPrice)) {
            isBtnDisabled = true;
        }
        else {
            cartForm.listProduct.forEach(Q => {
                if (Q.isValid == false) {
                    isBtnDisabled = true;
                    return;
                }
            });
        }

        setIsPayButtonDisabled(isBtnDisabled);
    }

    const onDriverChange = (selected) => {
        const newCartForm = cartForm;
        newCartForm.driver = selected;
        setCartForm({ ...newCartForm });
    }

    const onAddressChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        const value = e.target.value;
        const newCartForm = cartForm;
        newCartForm.address = value;
        setCartForm({ ...newCartForm });

        validateAddress();
    }

    const validateAddress = () => {
        if (cartForm.isCreateDelivery && (cartForm.address == '' || cartForm.address == undefined)) {
            setAddressErrMsg('Alamat tidak boleh kosong');
        }
        else {
            setAddressErrMsg('');
        }
    }

    const onInterestChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = e.target.value;
        const newForm = cartForm;
        newForm.interest = newValue;
        setCartForm({ ...newForm });

        validateInterest();
    }

    const validateInterest = () => {
        if (cartForm.paymentType.value == CREDIT_PAYMENT_TYPE && (cartForm.interest == null || cartForm.interest.trim() == '')) {
            setInterestErrMsg('Bunga harus diisi');
        }
        else {
            setInterestErrMsg('');
        }
    }

    const onDeadlineDateChange = (newDate) => {
        const newForm = cartForm;
        newForm.deadlineDate = newDate;
        setCartForm({ ...newForm });

        validateDeadlineDate();
    }

    const validateDeadlineDate = () => {
        if (cartForm.paymentType.value == CREDIT_PAYMENT_TYPE && (cartForm.deadlineDate == null)) {
            setDeadlineDateErrMsg('Batas waktu pembayaran harus diisi');
        }
        else {
            setDeadlineDateErrMsg('');
        }
    }

    const onBuyerNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = e.target.value;
        const newForm = cartForm;
        newForm.buyerName = newValue;
        setCartForm({ ...newForm });

        validateBuyerName();
    }

    const validateBuyerName = () => {
        if (cartForm.buyerName.trim() == '' || cartForm.buyerName == undefined) {
            setBuyerNameErrMsg('Nama pembeli tidak boleh kosong');
        }
        else {
            setBuyerNameErrMsg('');
        }
    }

    const calculateChange = () => {
        var newValue = 0;
        if (cartForm.buyerMoney.trim() != '' && parseInt(cartForm.buyerMoney) > 0 && parseInt(cartForm.buyerMoney) > cartForm.totalPrice) {
            newValue = parseInt(cartForm.buyerMoney) - cartForm.totalPrice;
        }

        return newValue;
    }

    const onBuyerMoneyChange = (newValue: string) => {
        const newForm = cartForm;
        newForm.buyerMoney = newValue;
        setCartForm({ ...newForm });

        validateBuyerMoney();
    }

    const validateBuyerMoney = () => {
        if (cartForm.buyerMoney == undefined || cartForm.buyerMoney.trim() == '') {
            setBuyerMoneyErrMsg('Pembayaran pembeli tidak boleh kosong');
        }
        else if (parseInt(cartForm.buyerMoney) < cartForm.totalPrice) {
            setBuyerMoneyErrMsg('Pembayaran pembeli harus lebih besar dari pada total harga');
        }
        else {
            setBuyerMoneyErrMsg('');
        }
        const newChange = calculateChange();

        const newForm = cartForm;
        newForm.buyerChange = newChange;
        setCartForm({ ...newForm });
    }

    const renderDeliveryForm = () => {
        if (cartForm.isCreateDelivery == 1) {
            return (
                <div className="card p-2 shadow-sm">
                    <div className='d-flex flex-row justify-content-between mb-1'>
                        <p className='m-0 my-auto p-0'>Nama Pengirim</p>
                        <div className='w-25'>
                            <Select options={listDriver} onChange={onDriverChange} value={cartForm.driver} autoFocus />
                        </div>
                    </div>
                    <div className='d-flex flex-row justify-content-between mb-1'>
                        <p className='m-0 my-auto p-0'>Alamat</p>
                        <div className='w-25'>
                            <textarea onChange={onAddressChange} className='d-flex w-100 form-control' value={cartForm.address} />
                            <small className='text-danger'>{addressErrMsg}</small>
                        </div>
                    </div>
                </div>
            );
        }

        return undefined;
    }

    const renderCreditPaymentForm = () => {
        if (cartForm.paymentType.value == CREDIT_PAYMENT_TYPE) {
            return (
                <div className="card p-2 shadow-sm">
                    <div className='d-flex flex-row justify-content-between mb-1'>
                        <p className='m-0 my-auto p-0'>Bunga (%)</p>
                        <div className='w-25'>
                            <input type="number" className='form-control' value={cartForm.interest} onChange={onInterestChange} />
                            <small className='text-danger'>{interestErrMsg}</small>
                        </div>
                    </div>
                    <div className='d-flex flex-row justify-content-between mb-1'>
                        <p className='m-0 my-auto p-0'>Batas waktu pembayaran</p>
                        <div className='w-25'>
                            <SingleDatePicker
                                date={cartForm.deadlineDate}
                                onDateChange={(date) => onDeadlineDateChange(date)}
                                focused={focused}
                                onFocusChange={(f) => setFocused(f.focused)}
                                block={true}
                                small={true}
                                id="deadlineDatePicker"
                            />
                            <small className='text-danger'>{deadlineDateErrMsg}</small>
                        </div>
                    </div>
                </div>
            );
        }

        return undefined;
    }

    const resetForm = () => {
        setCartForm({
            isCreateDelivery: 0,
            paymentType: {
                label: listPaymentType[listPaymentType.length - 1]?.label as string,
                value: listPaymentType[listPaymentType.length - 1]?.value
            },
            buyerChange: 0,
            totalPrice: 0,
            buyerMoney: '0',
            address: undefined,
            buyerName: '',
            deadlineDate: null,
            driver: {
                label: listDriver[0]?.label as string,
                value: listDriver[0]?.value
            },
            interest: undefined,
            listProduct: []
        });

        removeCookie('cashier');
    }

    return (
        <div>
            <LoadingPage loading={isLoading} />
            <h1>Kasir</h1>
            <hr />
            <CashierModal form={cartForm} onSuccessPaid={resetForm} setShowLoading={isShow => setIsLoading(isShow)}></CashierModal>
            <div className='card shadow-sm p-2 mb-3'>
                <InputBarcodeOrName onGetValidInput={onGetValidInput} />
                <div className='mt-5 border border-1 border-dark'>
                    <CartProductGridData onRemoveProductClick={onRemoveProductClick} onChange={onChange} listProduct={cartForm.listProduct}></CartProductGridData>
                </div>
                <div className='d-flex flex-row mt-3 justify-content-end'>
                    <p className='fw-bold me-1'>Total Biaya : </p>
                    <p className='fw-bold'><PriceFormat value={cartForm.totalPrice} /></p>
                </div>
                <hr />
                <div className='d-flex w-75 mb-2'>
                    <label className="form-check-label fw-bold col-4 my-auto">Nama pembeli</label>
                    <div className='w-50'>
                        <input type="text" className='form-control' value={cartForm.buyerName} onChange={onBuyerNameChange} />
                        <small className='text-danger'>{buyerNameErrMsg}</small>
                    </div>
                </div>
                <br />
                {cartForm.paymentType.value == PaymentTypeEnum.TUNAI ?
                    <div>
                        <div className='d-flex w-75 mb-2'>
                            <label className="form-check-label fw-bold col-4 my-auto">Total Pembayaran</label>
                            <div className='w-50'>
                                
                                <PriceInput returnType={ReturnType.STRING} className='form-control' value={parseInt(cartForm.buyerMoney)} onChange={onBuyerMoneyChange} />
                                <small className='text-danger'>{buyerMoneyErrMsg}</small>
                            </div>
                        </div>
                        <br />
                    </div> : undefined}
                <div className='d-flex w-75 mb-2'>
                    <p className='my-auto fw-bold col-4'>Pilih metode pembayaran</p>
                    <div className='w-50 col-4'>
                        <Select options={listPaymentType} onChange={onPaymentTypeChange} value={cartForm.paymentType} />
                    </div>
                </div>
                {renderCreditPaymentForm()}
                <br />
                <div className='d-flex w-75 mb-2'>
                    <label className="form-check-label fw-bold col-4">Apakah perlu pengiriman?</label>
                    <div className='d-flex col-4' onChange={onCreateDeliveryChange}>
                        <div className="form-check me-4">
                            <input className="form-check-input" type="radio" name="isCreateDeliveryRadio" value={1} checked={cartForm.isCreateDelivery == 1} onChange={() => { }} />
                            <label className="form-check-label">Ya</label>
                        </div>
                        <div className="form-check">
                            <input className="form-check-input" type="radio" name="isCreateDeliveryRadio" value={0} checked={cartForm.isCreateDelivery == 0} onChange={() => { }} />
                            <label className="form-check-label">Tidak</label>
                        </div>
                    </div>
                </div>
                {renderDeliveryForm()}
                <br />
                {cartForm.paymentType.value == PaymentTypeEnum.TUNAI ?
                    <div>
                        <div className='d-flex w-75 mb-2'>
                            <label className="form-check-label fw-bold col-4 my-auto">Kembalian</label>
                            <div className='w-50'>
                                <p><PriceFormat value={cartForm.buyerChange} /></p>
                            </div>
                            <br />
                        </div>
                    </div>
                    : undefined}
                <hr />
                <button className='btn btn-primary ms-auto d-flex' disabled={isPayButtonDisabled} type="button" data-bs-toggle="modal" data-bs-target="#cashierStaticBackdrop">Bayar</button>
            </div>
        </div>
    )
}

export default function CashierPage() {
    return (
        <Authorize onlyLoggedIn={true}>
            <Layout title='cashier'>
                <Cashier></Cashier>
            </Layout>
        </Authorize>
    );
}