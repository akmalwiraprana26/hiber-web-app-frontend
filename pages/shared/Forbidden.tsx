import React from "react"
import Layout from "./Layout"

const Forbidden: React.FunctionComponent<{}> = (props) => {
    return (
        <div className='text-center w-50 m-auto'>
            <h1 className='display-1'>403</h1>
            <h3 className='fw-bold'>ACCESS DENIED</h3>
            <br />
            <p className='m-0'>Maaf anda tidak dapat mengakses halaman ini</p>
            <p className='m-0'>Halaman ini hanya dapat diakses oleh user dengan role tertentu</p>
        </div>
    )
}

export default function ForbiddenPage() {
    return (
        <Layout title={"Forbidden"}>
            <Forbidden></Forbidden>
        </Layout>
    )
};
