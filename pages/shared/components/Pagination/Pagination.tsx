import React, {useState, useEffect} from 'react';

/**
 * Pagination component
 * @param param0 
 * @returns 
 */
export const Pagination: React.FunctionComponent<{
    currentPage: number;
    offsetRange?: number;
    dataLength: number;
    dataPerPage: number;
    onPageChange: (number: number) => void;
}> = function ({currentPage, dataLength, dataPerPage, offsetRange = 2, onPageChange}) {
    const [pageCount, setPageCount] = useState(0);

    const pageNumbers: number[] = [];
    const shownNumbers: number[] = [];
    let firstNumber;
    let lastNumber;

    useEffect(() => {
        if(dataPerPage === 0){
            setPageCount(1);
        }else{
            setPageCount(Math.ceil(dataLength / dataPerPage));
        }
    });

    if(currentPage - offsetRange > 1){
        firstNumber = (
            <React.Fragment>
                <li className="page-item">
                    <a onClick={() => onPageChange(1)} className={"page-link " + (currentPage == 1 ? "active" : "")}>
                        1
                    </a>
                </li>
                <li className="page-item disabled">
                    <a className="page-link">...</a>
                </li>
            </React.Fragment>                       
        )
    }
    if(currentPage + offsetRange < pageCount){
        lastNumber = (
            <React.Fragment>
                <li className="page-item disabled">
                    <a className="page-link">...</a>
                </li>
                <li className="page-item">
                    <a onClick={() => onPageChange(pageCount)} className={"page-link " + (currentPage == pageCount ? "active" : "")}>
                        {pageCount}
                    </a>
                </li>
            </React.Fragment>                
        )
    }

    for(let i = 1 ; i <= pageCount; i++){
        pageNumbers.push(i);
        if(i >= currentPage - offsetRange && i <= currentPage + offsetRange ){
            shownNumbers.push(i);
        }   
    }

    return (
        <nav className="my-3" style={{cursor: 'pointer'}}>
            <ul className="pagination m-0">
                <li className={"page-item " + ((currentPage == 1 || pageCount == 0) ? "disabled" : "")}>
                    <a onClick={() => onPageChange(currentPage - 1)} className="page-link">
                        Sebelumnya
                    </a>
                </li>
                {firstNumber}
                {
                    shownNumbers.map(number =>(
                        <li key={number} className="page-item">
                            <a onClick={() => onPageChange(number)} className={"page-link " + (currentPage == number ? "active fw-bold" : "")}>
                                {number}
                            </a>
                        </li>                          
                    ))
                }
                {lastNumber}
                <li className={"page-item " + ((currentPage == pageCount || pageCount == 0) ? "disabled" : "")}>
                    <a onClick={() => onPageChange(currentPage + 1)} className="page-link">
                        Selanjutnya
                    </a>
                </li>
            </ul>
        </nav>
    );
}

export default Pagination;