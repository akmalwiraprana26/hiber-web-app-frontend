import React, { useState, useEffect } from "react";
import Swal from "sweetalert2";
import { TransactionDetailViewModel, TransactionProductDetail, ReturProductClient, ReturProductDetail, UserClient, UserViewModel } from "../../../../api/hiber-api";
import Select from 'react-select'
import { DropdownModel } from "../../../transaction";

interface FormReturProductModel {
    productId: string;
    productName: string;
    quantity: string;
    error: string;
}

interface DeliveryFormModel {
    driver: DriverDropdownModel | undefined;
    address: string;
}

interface FormRetur {
    formReturProduct: FormReturProductModel[];
    formdelivery: DeliveryFormModel;
}

export interface DriverDropdownModel {
    label: string;
    value: string | undefined;
}

const ReturTransactionModal: React.FunctionComponent<{
    transactionId: string,
    detail: TransactionDetailViewModel,
    setShowLoading: (isShow: boolean) => void,
    onRetur: () => void
}> = (props) => {
    const [isReturButtonDisabled, setIsReturButtonDisabled] = useState<boolean>(false);
    const [isCreateDelivery, setIsCreateDelivery] = useState<boolean>(false);
    const [listBoughtProduct, setListBoughtProduct] = useState<TransactionProductDetail[]>([]);
    const [formRetur, setFormRetur] = useState<FormRetur>({
        formReturProduct: [],
        formdelivery: {
            address: '',
            driver: undefined
        }
    })
    const [listDriver, setListDriver] = useState<DriverDropdownModel[]>();
    const [addressErrMsg, setAddressErrMsg] = useState<string>('');

    useEffect(() => {
        setListBoughtProduct(props.detail.listBoughtProduct as TransactionProductDetail[]);
    }, []);

    useEffect(() => {
        initReturList();
        initDriverList();
    }, [listBoughtProduct]);

    const initDriverList = async () => {
        props.setShowLoading(true);
        const client = new UserClient();
        try {
            const data: UserViewModel[] = await client.getAllDriver();
            const driverDropdown: DriverDropdownModel[] = data.map(Q => {
                return {
                    label: Q.name as string,
                    value: Q.userId
                }
            });
            setListDriver(driverDropdown);
        } catch (error) {
            
        } finally {
            props.setShowLoading(false);
        }
    }

    const initReturList = () => {
        let newList: FormReturProductModel[] = []; 
        listBoughtProduct.forEach(Q => {
            newList.push({
                productId: Q.productId,
                productName: Q.productName ?? '',
                quantity: Q.quantity.toString(),
                error: ''
            });
        });
        setListReturProduct([...newList]);
    }

    const setListReturProduct = (data: FormReturProductModel[]) => {
        setFormRetur({
            ...formRetur,
            formReturProduct: data
        });
    }

    const updateNewListRetur = (value: string, index: number) => {
        let newList: FormReturProductModel[] = [];
        let thereIsError = false;
        formRetur.formReturProduct.forEach((Q, currIndex) => {
            if(currIndex == index) {
                let newData = Q;
                newData.quantity = value;
                if(value == '' || parseInt(Q.quantity) < 0 || parseInt(Q.quantity) > (listBoughtProduct[index]?.quantity ?? 0)) {
                    newData.error = 'Input Invalid';
                    thereIsError = true;
                }
                else {
                    newData.error = '';
                }
                newList.push({
                    ...newData
                });
            }
            else {
                newList.push({...Q});
            }
        });
        setListReturProduct([...newList]);
        if(thereIsError) {
            setIsReturButtonDisabled(true);
        }
        else {
            setIsReturButtonDisabled(false);
        }
    }

    const onInputChange = (e: React.ChangeEvent<HTMLInputElement>, index: number) => {
        const value = e.target.value;
        updateNewListRetur(value, index);
    }

    const renderReturForm = () => {
        if(formRetur.formReturProduct != []) {
            const list = formRetur.formReturProduct;
            const form = list.map((Q, index) => {
                return (
                    <div key={Q.productId} className='d-flex flex-column'>
                        <div className='flex-row d-flex justify-content-between mb-1'>
                            <p className='m-0 my-auto p-0 w-25'>{Q.productName}</p>
                            <p className='m-0 my-auto p-0 w-25'>{listBoughtProduct[index]?.quantity}</p>
                            <input type="number" className='w-25' placeholder={Q.quantity} value={formRetur.formReturProduct[index]?.quantity} onChange={(e) => onInputChange(e, index)}/>
                        </div>
                        <span className='ms-auto text-danger'>{formRetur.formReturProduct[index]?.error}</span>
                        <hr />
                    </div>
                )
            });
            return form;
        }

        return undefined;
    }

    const resetForm = () => {
        initReturList();
        resetDeliveryForm();
        props.onRetur();
    }

    const resetDeliveryForm = () => {
        const drivers = listDriver as DriverDropdownModel[];
        const defaultDriver = drivers[0];
        formRetur.formdelivery = {
            address: '',
            driver: defaultDriver
        }
    }

    const retur = async () => {
        setIsReturButtonDisabled(true);
        props.setShowLoading(true);
        try {
            const client = new ReturProductClient();
            let productDatas: ReturProductDetail[] = [];
            formRetur.formReturProduct.forEach(Q => {
                if(parseInt(Q.quantity) > 0) {
                    productDatas.push({
                        productId: Q.productId,
                        productQuantity: parseInt(Q.quantity)
                    });
                }
            })
            
            const success = await client.returTransaction({
                isCreateDelivery: isCreateDelivery,
                transactionId: props.transactionId,
                products: productDatas,
                address: isCreateDelivery ? formRetur.formdelivery.address : undefined,
                driverId: isCreateDelivery ? formRetur.formdelivery.driver?.value : undefined
            });
            if(success) {
                Swal.fire({
                    title: 'Berhasil melakukan retur',
                    text: 'Retur berhasil',
                    icon: 'success'
                });

                resetForm();
            }
            else {
                Swal.fire({
                    title: 'Gagal melakukan retur',
                    text: 'Retur gagal',
                    icon: 'error'
                });
            }
        } catch (error) {
            Swal.fire({
                title: 'Gagal melakukan retur',
                text: 'Retur gagal',
                icon: 'error'
            });
        } finally {
            setIsReturButtonDisabled(false);
            props.setShowLoading(false);
        }
    }

    const onCreateDeliveryChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setIsCreateDelivery(!isCreateDelivery);
        setIsReturButtonDisabled(true);
        resetDeliveryForm();
    }

    const onDriverChange = (selected) => {
        const newFormDelivery = {
            ...formRetur.formdelivery,
        }
        newFormDelivery.driver = selected;
        setFormRetur({
            ...formRetur,
            formdelivery: newFormDelivery
        });
    }

    const onAddressChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        const value = e.target.value;
        const newFormRetur = {
            ...formRetur
        }
        newFormRetur.formdelivery.address = value;
        setFormRetur({
            ...newFormRetur
        });

        validateAddress();
    }

    const validateAddress = () => {
        if(isCreateDelivery && (formRetur.formdelivery.address == '' || formRetur.formdelivery.address == undefined)) {
            setAddressErrMsg('Alamat tidak boleh kosong');
            setIsReturButtonDisabled(true);
        }
        else {
            setAddressErrMsg('');
            setIsReturButtonDisabled(false);
        }
    }

    const renderCreateDeliveryForm = () => {
        return (
            <div className='d-flex flex-column'>
                <div className='d-flex flex-row justify-content-between mb-1'>
                    <p className='m-0 my-auto p-0'>Nama Pengirim</p>
                    <div className='w-50'>
                        <Select options={listDriver} onChange={onDriverChange} value={formRetur.formdelivery.driver} autoFocus/>
                    </div>
                </div>
                <div className='d-flex flex-row justify-content-between mb-1'>
                    <p className='m-0 my-auto p-0'>Alamat</p>
                    <div className='w-50'>
                        <textarea onChange={onAddressChange} className='d-flex w-100' value={formRetur.formdelivery.address}/>
                    </div>
                </div>
                <span className='ms-auto text-danger'>{addressErrMsg}</span>
            </div>
        );
    }

    return (
        <div>
            <div className="modal fade" id="returStaticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex={-1} aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="staticBackdropLabel">Retur</h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={resetForm}></button>
                    </div>
                    <div className="modal-body">
                        <div className='flex-row d-flex justify-content-between'>
                            <p className='m-0 p-0 w-25 fw-bold'>Nama Barang</p>
                            <p className='m-0 p-0 w-25 fw-bold'>Max Retur</p>
                            <p className='m-0 p-0 w-25 fw-bold'>Jumlah Retur</p>
                        </div>
                        <hr />
                        {renderReturForm()}
                        <div className="form-check">
                            <input className="form-check-input" type="checkbox" onChange={onCreateDeliveryChange} id="flexCheck"/>
                            <label className="form-check-label" htmlFor="flexCheck">
                                Buat Pengiriman
                            </label>
                        </div>
                        <br />
                        {isCreateDelivery ? renderCreateDeliveryForm() : undefined}
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal" onClick={resetForm}>Batal</button>
                        <button type="button" className="btn btn-primary" disabled={isReturButtonDisabled} onClick={retur} data-bs-dismiss="modal">Retur</button>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ReturTransactionModal;