import React, { useState, useEffect } from "react";
import Swal from "sweetalert2";
import { TransactionDetailViewModel, PurchaseOrderDetailViewModel, ReturProductClient, ReturProductDetail, UserClient, UserViewModel, PurchaseOrderClient, ReturPurchaseOrderDetail } from "../../../../api/hiber-api";
import Select from 'react-select'
import { DropdownModel } from "../../../transaction";

interface FormReportPurchaseOrderModel {
    productId: string;
    productName: string;
    maxQuantity: string;
    quantity: string;
    error: string;
}

const ReportPOModal: React.FunctionComponent<{
    purchaseOrderId: string,
    listPOProduct: PurchaseOrderDetailViewModel[],
    onReport: () => void
}> = (props) => {
    const [isReportButtonDisabled, setIsReportButtonDisabled] = useState<boolean>(false);
    const [listReportPOProduct, setListReportPOProduct] = useState<FormReportPurchaseOrderModel[]>([]);

    useEffect(() => {
        initReportList();
    }, [props.listPOProduct]);

    const initReportList = () => {
        const listReport = props.listPOProduct.map(Q => {
            return {
                productId: Q.productId,
                productName: Q.productName,
                maxQuantity: Q.quantity.toString(),
                quantity: (0).toString(),
                error: ''
            } as FormReportPurchaseOrderModel
        });

        setListReportPOProduct([...listReport]);
    }

    const updateNewListReport = (value: string, index: number) => {
        let newList: FormReportPurchaseOrderModel[] = [];
        let thereIsError = false;
        listReportPOProduct.forEach((Q, currIndex) => {
            if(currIndex == index) {
                let newData = Q;
                newData.quantity = value;
                if(value == '' || parseInt(Q.quantity) < 0 || parseInt(Q.quantity) > (listReportPOProduct[index]?.maxQuantity ?? 0)) {
                    newData.error = 'Input Invalid';
                    thereIsError = true;
                }
                else {
                    newData.error = '';
                }
                newList.push({
                    ...newData
                });
            }
            else {
                newList.push({...Q});
            }
        });
        setListReportPOProduct([...newList]);
        if(thereIsError) {
            setIsReportButtonDisabled(true);
        }
        else {
            setIsReportButtonDisabled(false);
        }
    }

    const onInputChange = (e: React.ChangeEvent<HTMLInputElement>, index: number) => {
        const value = e.target.value;
        updateNewListReport(value, index);
    }

    const renderReportForm = () => {
        if(listReportPOProduct != []) {
            const list = listReportPOProduct;
            const form = list.map((Q, index) => {
                return (
                    <div key={Q.productId} className='d-flex flex-column'>
                        <div className='flex-row d-flex justify-content-between mb-1'>
                            <p className='m-0 my-auto p-0 w-25'>{Q.productName}</p>
                            <p className='m-0 my-auto p-0 w-25'>{listReportPOProduct[index]?.maxQuantity}</p>
                            <input type="number" className='w-25' placeholder={Q.quantity} value={listReportPOProduct[index]?.quantity} onChange={(e) => onInputChange(e, index)}/>
                        </div>
                        <span className='ms-auto text-danger'>{listReportPOProduct[index]?.error}</span>
                        <hr />
                    </div>
                )
            });
            return form;
        }

        return undefined;
    }

    const resetForm = () => {
        initReportList();
        props.onReport();
    }

    const report = async () => {
        setIsReportButtonDisabled(true);
        try {
            const client = new PurchaseOrderClient();
            let productDatas: ReturPurchaseOrderDetail[] = [];
            listReportPOProduct.forEach(Q => {
                if(parseInt(Q.quantity) != 0) {
                    productDatas.push({
                        productId: Q.productId,
                        productQuantity: parseInt(Q.quantity)
                    });
                }
            })
            
            const success = await client.reportDamagedPurchaseOrder({
                purchaseOrderId: props.purchaseOrderId,
                products: productDatas,
            });
            if(success) {
                Swal.fire({
                    title: 'Berhasil menyimpan data purchase order',
                    text: 'Data berhasil disimpan',
                    icon: 'success'
                });

                resetForm();
            }
            else {
                Swal.fire({
                    title: 'Gagal menyimpan data purchase order',
                    text: 'Data gagal disimpan',
                    icon: 'error'
                });
            }
        } catch (error) {
            Swal.fire({
                title: 'Gagal menyimpan data purchase order',
                text: 'Data gagal disimpan',
                icon: 'error'
            });
        } finally {
            setIsReportButtonDisabled(false);
        }
    }

    return (
        <div>
            <div className="modal fade" id="reportPOStaticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex={-1} aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="staticBackdropLabel">Purchase Order</h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={resetForm}></button>
                    </div>
                    <div className="modal-body">
                        <div className='flex-row d-flex justify-content-between'>
                            <p className='m-0 p-0 w-25 fw-bold'>Nama Barang</p>
                            <p className='m-0 p-0 w-25 fw-bold'>Max Kuantitas</p>
                            <p className='m-0 p-0 w-25 fw-bold'>Jumlah Barang Rusak</p>
                        </div>
                        <hr />
                        {renderReportForm()}
                        <br />
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal" onClick={resetForm}>Batal</button>
                        <button type="button" className="btn btn-primary" disabled={isReportButtonDisabled} onClick={report} data-bs-dismiss="modal">Simpan</button>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ReportPOModal;