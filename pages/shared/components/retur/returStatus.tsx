const ReturStatus: React.FunctionComponent<{
    className?: string
}> = (props) => {
    return (
        <div>
            <span className={"badge bg-dark rounded-pill p-2 "+props.className}>Dalam proses retur</span>
        </div>
    );
}

export default ReturStatus;