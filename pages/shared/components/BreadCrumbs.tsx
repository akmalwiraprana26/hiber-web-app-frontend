import { useRouter } from "next/dist/client/router";
import React from "react"
import Swal from "sweetalert2";

const BreadCrumbs : React.FunctionComponent<{
    text: string,
    href: string,
    isChanged: boolean
}> = (props) =>{
    const Router = useRouter();
    const onClick = () =>{
        if(props.isChanged){
            Swal.fire({
                icon: 'warning',
                title: "<span>Tutup</span>",
                html: 'Apakah anda yakin menutup<br/><span>halaman ini dan kembali ke halaman sebelumnya?</span> <br/><br/><span class="text-danger">* Data yang belum disimpan akan hilang</span>',
                showCancelButton: true,
                confirmButtonText:
                    'Ya',
                cancelButtonText:
                    'Tidak',
            }).then((result) => {
                if(result.isConfirmed){
                    Router.push(props.href);
                }
            })
        }else{
            Router.push(props.href);
        }
    }

    return(
        <div className="mb-3">
            <a className="text-decoration-none pointer fw-bold" style={{color:'#E5B199'}} onClick={() => onClick()}>{"<"} {props.text}</a>
        </div>
    )
}

export default BreadCrumbs;