import { useRouter } from "next/dist/client/router";

const UnitIcon: React.FunctionComponent<{}> = (props) => {
    const router = useRouter();
    const active = (router.pathname.includes('/unit'));
    const getColor = () => {
        if(active) {
            return '#ffffff'
        }
        else {
            return '#000'
        }
    }
    return (
        <div className='navigation'>
            <svg fill={getColor()} version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" xmlnsXlink="http://www.w3.org/1999/xlink" enableBackground="new 0 0 512 512">
                <g>
                    <path d="m495.1,140.6l-84.9-85.4c-7.7-7.7-21.3-7.7-29,0l-159.4,160.5v-184.3c0-11.3-9.1-20.4-20.4-20.4h-170c-11.3,0-20.4,9.1-20.4,20.4v449.2c0,11.3 9.1,20.4 20.4,20.4h169.9c11.3,0 20.4-9.1 20.4-20.4v-72.8l39.9-8.3c3.9-0.8 7.5-2.8 10.3-5.6l223-224.4c8.1-8.1 8.1-20.9 0.2-28.9zm-314.2,319.6h-129.1v-55.5h29c11.3,0 20.4-9.1 20.4-20.4 0-11.3-9.1-20.4-20.4-20.4h-29v-87.4h29c11.3,0 20.4-9.1 20.4-20.4 0-11.3-9.1-20.4-20.4-20.4h-29v-87.4h29c11.3,0 20.4-9.1 20.4-20.4 0-11.3-9.1-20.4-20.4-20.4h-29v-55.7h129.1v204.9l-22.7,22.9c-2.9,2.9-4.9,6.6-5.6,10.7l-19.7,107.2c-1.2,6.7 1,13.5 5.8,18.3 7.3,7.3 17.1,5.7 18.4,5.4l23.8-5v44zm66.5-99.5l-68.9,14.4 13.1-71.1 204.1-205.4 56.1,56.4-204.4,205.7z"/>
                </g>
            </svg>
        </div>
    )
}

export default UnitIcon;