import { useRouter } from "next/dist/client/router";

const PurchaseOrderIcon: React.FunctionComponent<{}> = (props) => {
    const router = useRouter();
    const active = (router.pathname.includes('/purchaseOrder'));
    const getColor = () => {
        if(active) {
            return '#ffffff'
        }
        else {
            return '#000'
        }
    }
    return (
        <div className='navigation'>
            <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 44 49" className='me-3'>
                <g id="outlined" transform="translate(-7 -6)">
                    <g id="Marketing" transform="translate(8 -120)">
                        <g id="Bill" transform="translate(0 127)">
                            <path id="Rectangle-1590" d="M7,30.861V26.549m0-5.741V16.215M7,10.7V0H42V47H7V36.365" fill="none" stroke={getColor()} strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" fillRule="evenodd"/>
                            <rect id="Rectangle-1591" width="14" height="7" rx="3.5" transform="translate(0 10)" fill="none" stroke={getColor()} strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"/>
                            <rect id="Rectangle-1591-2" data-name="Rectangle-1591" width="14" height="7" rx="3.5" transform="translate(0 20)" fill="none" stroke={getColor()} strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"/>
                            <rect id="Rectangle-1591-3" data-name="Rectangle-1591" width="14" height="7" rx="3.5" transform="translate(0 30)" fill="none" stroke={getColor()} strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"/>
                            <rect id="Rectangle-1593" width="11" height="3" transform="translate(27 4)" fill="none" stroke={getColor()} strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"/>
                            <path id="Line" d="M17,11H38" fill="none" stroke={getColor()} strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" fillRule="evenodd"/>
                            <path id="Line-2" data-name="Line" d="M17,16H38" fill="none" stroke={getColor()} strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" fillRule="evenodd"/>
                            <path id="Line-3" data-name="Line" d="M17,21H38" fill="none" stroke={getColor()} strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" fillRule="evenodd"/>
                            <path id="Line-4" data-name="Line" d="M17,26H38" fill="none" stroke={getColor()} strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" fillRule="evenodd"/>
                            <path id="Line-5" data-name="Line" d="M17,31H38" fill="none" stroke={getColor()} strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" fillRule="evenodd"/>
                            <path id="Line-6" data-name="Line" d="M17,36H38" fill="none" stroke={getColor()} strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" fillRule="evenodd"/>
                            <path id="Line-7" data-name="Line" d="M17,41H38" fill="none" stroke={getColor()} strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" fillRule="evenodd"/>
                        </g>
                    </g>
                </g>
            </svg>
        </div>
    )
}

export default PurchaseOrderIcon;