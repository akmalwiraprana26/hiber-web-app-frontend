import { useRouter } from "next/dist/client/router";

const ProductIcon: React.FunctionComponent<{}> = (props) => {
    const router = useRouter();
    const active = (router.pathname.includes('/product'));
    const getColor = () => {
        if(active) {
            return '#ffffff'
        }
        else {
            return '#000'
        }
    }
    return (
        <div className='navigation'>
            <svg id="ad-product-svgrepo-com" xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 50 50" className='me-3'>
                <path id="Path_1" data-name="Path 1" d="M50,0H0V50H50Z" fill="rgba(255,255,255,0.01)"/>
                <path id="Path_2" data-name="Path 2" d="M46,14.5,25,4,4,14.5v21L25,46,46,35.5Z" transform="translate(0 0)" fill="none" stroke={getColor()} strokeLinejoin="round" strokeWidth="4"/>
                <path id="Path_3" data-name="Path 3" d="M4,14,24,24" transform="translate(0 0.737)" fill="none" stroke={getColor()} strokeLinecap="round" strokeLinejoin="round" strokeWidth="4"/>
                <path id="Path_4" data-name="Path 4" d="M24,44V24" transform="translate(1 2)" fill="none" stroke={getColor()} strokeLinecap="round" strokeLinejoin="round" strokeWidth="4"/>
                <path id="Path_5" data-name="Path 5" d="M44,14,24,24" transform="translate(2 0.737)" fill="none" stroke={getColor()} strokeLinecap="round" strokeLinejoin="round" strokeWidth="4"/>
                <path id="Path_6" data-name="Path 6" d="M34,9,14,19" transform="translate(1 0.474)" fill="none" stroke={getColor()} strokeLinecap="round" strokeLinejoin="round" strokeWidth="4"/>
            </svg>
        </div>
    )
}

export default ProductIcon;