import { DeliveryStatusEnum } from "../../../../enums/enums";

const DeliveryStatus: React.FunctionComponent<{
    type: number
}> = (props) => {
    const getStatusClassName = () => {
        if(props.type == DeliveryStatusEnum.MEMUAT_BARANG) {
            return 'belum-dikirim-style';
        }
        else if(props.type == DeliveryStatusEnum.MENUJU_CUSTOMER) {
            return 'pengiriman-style';
        }
        else if(props.type == DeliveryStatusEnum.BARANG_SAMPAI) {
            return 'sudah-dikirim-style';
        }

        return '';
    }
    const getStatus = () => {
        if(props.type == DeliveryStatusEnum.MEMUAT_BARANG) {
            return 'Belum dikirim';
        }
        else if(props.type == DeliveryStatusEnum.MENUJU_CUSTOMER) {
            return 'Pengiriman';
        }
        else if(props.type == DeliveryStatusEnum.BARANG_SAMPAI) {
            return 'Sudah dikirim';
        }

        return '';
    }
    return (
        <div>
            <span className={"badge rounded-pill p-2 "+getStatusClassName()}>{getStatus()}</span>
        </div>
    );
}

export default DeliveryStatus;