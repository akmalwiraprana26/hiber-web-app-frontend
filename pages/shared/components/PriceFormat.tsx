import NumberFormat from "react-number-format";

const PriceFormat: React.FunctionComponent<{
    value: string | number | null | undefined
}> = (props) => {
    return (
        <span>
            <NumberFormat value={props.value} prefix="Rp" thousandSeparator={"."} displayType={'text'} decimalSeparator=","/>
        </span>
    )
}

export default PriceFormat;