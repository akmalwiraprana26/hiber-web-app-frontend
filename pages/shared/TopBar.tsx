import LogoutIcon from "./components/icons/logoutIcon";
import Image from "next/image"
import React, { useEffect, useState } from "react";
import Link from "next/link";
import { useCookies } from "react-cookie";
import { useRouter } from "next/dist/client/router";

const TopBarItem: React.FunctionComponent<{}> = (props) => {
    return (
        <div className="my-auto p-2">
            {props.children}
        </div>
    );
}

const TopBar: React.FunctionComponent<{}> = (props) => {
    const [cookies, setCookie, removeCookie] = useCookies(['username', 'userId', 'roleId', 'roleName', 'cashier']);
    const [userId, setUserId] = useState<string>('');
    const [username, setUsername] = useState<string>('');
    const router = useRouter();

    useEffect(() => {
        setUsername(cookies.username);
        setUserId(cookies.userId);
    }, [])

    const tryLogout = () => {
        removeCookie('username');
        removeCookie('userId');
        removeCookie('roleId');
        removeCookie('roleName');
        removeCookie('cashier');
        
        router.push('/login');
    }
    
    return (
        <div className="d-flex flex-row position-sticky top-0 bg-light shadow-lg px-3 top-bar">
            <div className="d-flex flex-grow-1">
                <TopBarItem>
                    <Link href='/dashboard'>
                        <a>
                            <Image src='/assets/HI_Ber-removebg-preview.png' width='76' height='50' alt='temp'/>
                        </a>
                    </Link>
                </TopBarItem>
            </div>
            <div className="d-flex flex-row">
                <TopBarItem>
                    <a className="nav-link dropdown-toggle text-dark my-auto" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Hi, {username}
                    </a>
                    <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li>
                            <Link href={'/user/view/' + userId}>
                                <a className="dropdown-item">View Profil</a>
                            </Link>
                        </li>
                        <li>
                            <Link href={'/user/edit/' + userId}>
                            <a className="dropdown-item">Edit Profil</a>
                            </Link>
                        </li>
                    </ul>
                </TopBarItem>
                <TopBarItem>
                    <a className="p-0 m-0 text-dark d-flex flex-row logout-btn" onClick={tryLogout}>
                        <p className="p-0 my-auto me-1">Logout</p>
                        <LogoutIcon/>   
                    </a>
                </TopBarItem>
            </div>
        </div>
    );
}

export default TopBar;