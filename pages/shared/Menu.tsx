import { useRouter } from "next/dist/client/router";
import React, { useEffect, useState } from "react";
import Link from "next/link";
import { useCookies } from "react-cookie";
import UserIcon from "./components/icons/userIcon";
import UnitIcon from "./components/icons/unitIcon";
import CategoryIcon from "./components/icons/categoryIcon";
import SupplierIcon from "./components/icons/supplierIcon";
import { RoleEnum } from "../../enums/enums";
import ProductIcon from "./components/icons/productIcon";
import DeliveryIcon from "./components/icons/deliveryIcon";
import PurchaseOrderIcon from "./components/icons/purchaseOrderIcon";
import TransactionIcon from "./components/icons/transactionIcon";
import CashierIcon from "./components/icons/cashierIcon";

export const NavigationLink: React.FunctionComponent<{
    href: string
}> = (props) => {
    const router = useRouter();
    const active = (router.pathname.includes(props.href));

    const getNavigationLinkClassName = (isActive: boolean) => {
        if (isActive) {
            return "text-light nav-active nav-active-bg-color";
        }
        else {
            return "text-dark nav-inactive nav-inactive-bg-color";
        }
    }

    return (
        <Link href={props.href}>
            <a className={"fw-bold nav-link shadow-lg py-3 " + getNavigationLinkClassName(active)}>
                <div className='d-flex flex-row w-100'>
                    {props.children}
                </div>
            </a>
        </Link>
    );
};

const Menu: React.FunctionComponent<{}> = (props) => {
    const [cookies, setCookie, removeCookie] = useCookies(['username', 'userId', 'roleId', 'roleName']);
    const [roleId, setRoleId] = useState<number>(-1);
    useEffect(() => {
        setRoleId(cookies.roleId);
    }, [])
    return (
        <div className="h-100 shadow-lg">
            <NavigationLink href="/product">
                <ProductIcon />
                <p className='my-auto'>Produk</p>
            </NavigationLink>
            {roleId == RoleEnum.ADMIN ?
                <NavigationLink href="/delivery">
                    <DeliveryIcon />
                    <p className='my-auto'>Pengiriman</p>
                </NavigationLink> : undefined}
            <NavigationLink href="/purchaseOrder">
                <PurchaseOrderIcon />
                <p className='my-auto'>Purchase Order</p>
            </NavigationLink>
            {roleId == RoleEnum.ADMIN ?
                <NavigationLink href="/transaction">
                    <TransactionIcon />
                    <p className='my-auto'>Transaksi</p>
                </NavigationLink> : undefined}
            <NavigationLink href="/cashier">
                <CashierIcon />
                <p className='my-auto'>Kasir</p>
            </NavigationLink>
            {roleId == RoleEnum.ADMIN ?
                <NavigationLink href="/user">
                    <UserIcon />
                    <p className='my-auto'>User</p>
                </NavigationLink> : undefined}
            {roleId == RoleEnum.ADMIN ?
                <NavigationLink href="/unit">
                    <UnitIcon />
                    <p className='my-auto'>Satuan Ukur</p>
                </NavigationLink> : undefined}
            {roleId == RoleEnum.ADMIN ?
                <NavigationLink href="/category">
                    <CategoryIcon />
                    <p className='my-auto'>Kategori Barang</p>
                </NavigationLink> : undefined}
            {roleId == RoleEnum.ADMIN ?
                <NavigationLink href="/supplier">
                    <SupplierIcon />
                    <p className='my-auto'>Supplier</p>
                </NavigationLink> : undefined}
        </div>
    );
}

export default Menu;