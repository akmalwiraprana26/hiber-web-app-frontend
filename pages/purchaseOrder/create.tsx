import { faMinus, faPlus } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import React, { useEffect, useState } from "react"
import Layout from "../shared/Layout"
import Select from 'react-select';
import { ProductClient, ProductDropdownModel, PurchaseDetailCreateModel, PurchaseOrderClient, PurchaseOrderCreateModel, SupplierClient, SupplierViewModel } from "../../api/hiber-api"
import { useCookies } from "react-cookie"
import Joi from "joi"
import Swal from "sweetalert2"
import Authorize from "../shared/Authorize"
import LoadingPage from "../shared/components/LoadingPage"
import PriceFormat from "../shared/components/PriceFormat"
import BreadCrumbs from "../shared/components/BreadCrumbs"

interface DropdownModel {
    label: string;
    value: number | string | undefined;
}

interface FormState {
    supplierId: {
        error: string
    },
    purchaseOrderDetail: [{
        quantity: {
            error: string
        },
        price: {
            error: string
        },
        productId: {
            error: string
        }
    }]
}

interface DetailFormState {
    quantity: {
        error: string
    },
    price: {
        error: string
    },
    productId: {
        error: string
    }
}

interface errorList {
    quantity: string;
    price: string;
    productId: string;    
}

const CreatePurchaseOrder: React.FunctionComponent<{}> = (props) => {

    const [cookies, setCookie, removeCookie] = useCookies(['username', 'userId', 'roleId', 'roleName']);
    const [username, setUsername] = useState<string>('');
    const [isSubmitting, setIsubmitting] = useState(false);
    const [supplierOptions, setSupplierOptions] = useState<DropdownModel[]>([]);
    const [selectedSupplier, setSelectedSupplier] = useState<DropdownModel>({
        label: '',
        value: 0
    });
    const [productOptions, setProductOptions] = useState<DropdownModel[]>([]);
    const [selectedProduct, setSelectedProduct] = useState<DropdownModel[]>([{
        label: '',
        value: ''
    }])
    const [form, setForm] = useState<PurchaseOrderCreateModel>({
        totalPrice: 0,
        supplierId: '',
        submitBy: '',
        purchaseOrderDetail: [{
            quantity: 0,
            price: 0,
            productId: ''
        }]
    });
    const [isChanged, setIsChanged] = useState(false);
    const [detailFormState, setDetailFormState] = useState<DetailFormState[]>([{
        quantity: {
            error: ''
        },
        price: {
            error: ''
        },
        productId: {
            error: ''
        }
    }])
    const [formState, setFormState] = useState<FormState>({
        supplierId: {
            error: ''
        },
        purchaseOrderDetail: [{
            quantity: {
                error: ''
            },
            price: {
                error: ''
            },
            productId: {
                error: ''
            }
        }]
    });
    const [detailForm, setDetailForm] = useState<PurchaseDetailCreateModel[]>([{ quantity: 0, price: 0, productId: "" }]);

    const onArrayChange = (key, index) => (e: React.ChangeEvent<HTMLInputElement>) => {
        setIsChanged(true)
        const value = e.target.value;
        let newDetailForm = detailForm.map((item, i) => {
            if (index === i) {
                return { ...item, [key]: value };
            } else {
                return item;
            }
        });
        setDetailForm(newDetailForm);
    }

    useEffect(() => {
        countTotalPrice();
    }, [detailForm])

    const countTotalPrice = () => {
        var total: number | undefined = 0;

        total = detailForm.reduce((sum, i) => (
            sum + (i.quantity * i.price)
        ), 0)
        var newForm = form;
        newForm.totalPrice = total;
        setForm({ ...newForm });
    }

    const addInputFields = () => {
        setDetailForm([...detailForm, { quantity: 0, price: 0, productId: "" }]);
        setSelectedProduct([...selectedProduct, { label: "", value: '' }]);
        let newFormState = formState;
        newFormState.purchaseOrderDetail.push({
            quantity: {
                error: ''
            },
            price: {
                error: ''
            },
            productId: {
                error: ''
            }
        })
        setFormState(newFormState);
    }

    const removeFormFields = (i) => {
        let newFormValues = [...detailForm];
        let newSelectedProduct = [...selectedProduct];
        let newFormState = formState;
        newFormState.purchaseOrderDetail.splice(i, 1);
        newFormValues.splice(i, 1);
        newSelectedProduct.splice(i, 1);
        setDetailForm(newFormValues);
        setSelectedProduct(newSelectedProduct);
        setFormState(newFormState);
    }

    const onSupplierChange = (selected) => {
        setIsChanged(true)
        setSelectedSupplier(selected);
        let newForm = form;
        newForm.supplierId = selected.value.toString();
        setForm({ ...newForm });
    }

    const onProductChange = (selected, index) => {
        setIsChanged(true)
        var newSelectedProduct: DropdownModel[] = selectedProduct;
        newSelectedProduct[index] = selected;
        setSelectedProduct([...newSelectedProduct]);

        const value = selected.value;
        let newDetailForm = detailForm.map((item, i) => {
            if (index === i) {
                return { ...item, ['productId']: value };
            } else {
                return item;
            }
        });
        setDetailForm(newDetailForm);
    }

    const fetchData = async () => {
        setIsubmitting(true);
        const supplierClient = new SupplierClient();
        const productClient = new ProductClient();
        try {
            const supplierResp: SupplierViewModel[] = await supplierClient.get();
            let supplierOptions: {}[] = [];
            supplierResp.forEach(Q => {
                supplierOptions.push({
                    label: Q.name,
                    value: Q.supplierId
                });
            })
            const productResp: ProductDropdownModel[] = await productClient.getProductList();
            let productOptions: {}[] = [];
            productResp.forEach(Q => {
                productOptions.push({
                    label: Q.productName,
                    value: Q.productID
                });
            })
            setSupplierOptions(supplierOptions as DropdownModel[]);
            setProductOptions(productOptions as DropdownModel[]);
        } catch (e) {

        } finally {
            setIsubmitting(false);
        }
    }

    const validateFormDetail = (formDetail: PurchaseDetailCreateModel[], key?: string) => {
        const Joi = require('joi');

        const schema: {
            quantity: Joi.SchemaLike,
            price: Joi.SchemaLike,
            productId: Joi.SchemaLike,
        } = {
            quantity: Joi.number().min(1).messages({
                'number.base': 'Kuantitas harus angka',
                'number.min': 'Kuantitas tidak boleh 0',
            }),
            price: Joi.number().min(1).messages({
                'string.base': 'Harga harus angka',
                'number.min': 'Harga tidak boleh 0',
            }),
            productId: Joi.string().empty().messages({
                'string.empty': 'Product harus diisi',
            })
        }

        const errorMessageArray: errorList[] = []

        for(let i=0; i<formDetail.length; i++) {
            const validationResult = Joi.object(schema).validate(formDetail[i], {
                abortEarly: false
            });
    
            if(validationResult.error == undefined || validationResult.error.details == undefined) {
                errorMessageArray.push({
                    quantity: '',
                    price: '',
                    productId: ''
                })
            }
            else {
                const err = validationResult.error.details;
                
                let errorMsg: errorList = {
                    price: '',
                    quantity: '',
                    productId: ''
                };

                for(let i = 0; i < err.length; i++) {
                    errorMsg[err[i].path[0]] = err[i].message;
                }
                
                errorMessageArray.push(errorMsg)    
            }
        } 

        return errorMessageArray;
    }

    const validateForm = (form, key?: string, index?) => {
        const schema: {
            supplierId: Joi.SchemaLike,
        } = {
            supplierId: '',
        }

        const errorMessage = {
            supplierId: '',
        }

        if (!key || key === 'supplierId') {
            schema['supplierId'] = Joi.string()
                .empty()
                .messages({
                    'string.empty': 'Supplier harus diisi',
                });
        }

        const validationResult = Joi.object(schema).validate(form, {
            abortEarly: false
        });

        const err = validationResult.error;

        if (!err) {
            return undefined;
        }

        for (const detail of err.details) {
            const key = detail.path[0]?.toString() ?? '';
            errorMessage[key] = detail.message;
        }

        return errorMessage;
    }

    const onSubmit = async (e: React.ChangeEvent<HTMLFormElement>) => {
        e.preventDefault();
        let newForm: PurchaseOrderCreateModel = form;
        newForm.submitBy = username;
        newForm.purchaseOrderDetail = detailForm;
        setForm({ ...newForm });

        const formToValidate = {
            supplierId: form.supplierId,
        }
        let isDetailValid = true;
        let formDetailToValidate: PurchaseDetailCreateModel[] = detailForm
        const validationResult = validateForm(formToValidate);
        const validationDetailResult = validateFormDetail(formDetailToValidate) as errorList[];
        for(let i = 0; i < validationDetailResult.length; i++) {
            if(validationDetailResult[i]?.price !== '' || validationDetailResult[i]?.quantity !== '' || validationDetailResult[i]?.productId !== ''){
                isDetailValid = false;
            }
        }
        if (validationResult != undefined || isDetailValid == false) {
            const newRegisterFormError: FormState = formState;
            const newDetailFormError: DetailFormState[] = detailFormState;
            for (const key in newRegisterFormError) {
                if (validationResult && validationResult[key]) {
                    newRegisterFormError[key].error = validationResult[key];
                }
                else {
                    newRegisterFormError[key].error = '';
                }
            }
            
            for(let i = 0; i < validationDetailResult.length; i++) {
                newRegisterFormError.purchaseOrderDetail[i] = {
                    price: {
                        error : validationDetailResult[i]?.price ?? ''
                    },
                    quantity: {
                        error : validationDetailResult[i]?.quantity ?? ''
                    },
                    productId: {
                        error : validationDetailResult[i]?.productId ?? ''
                    }
                }
            }

            setFormState({
                ...newRegisterFormError
            });
        }
        else {
            setIsubmitting(true);
            try {
                const client = new PurchaseOrderClient
                const success = await client.createPurchaseOrder(form);
                if (success) {
                    Swal.fire({
                        title: 'Berhasil membuat Purchase Order',
                        text: 'Purchase Order berhasil ditambahkan',
                        icon: 'success'
                    });

                    resetForm();
                    setIsChanged(false)
                }
                else {
                    Swal.fire({
                        title: 'Gagal membuat Purchase Order',
                        text: 'Sebuah kesalahan telah terjadi. Silakan coba lagi atau hubungi administrator',
                        icon: 'error'
                    });
                }
            } catch (error) {
                Swal.fire({
                    title: 'Gagal membuat Purchase Order',
                    text: 'Sebuah kesalahan telah terjadi. Silakan coba lagi atau hubungi administrator',
                    icon: 'error'
                });
            } finally {
                setIsubmitting(false);
            }
        }
    }

    const resetForm = () => {
        setForm({
            totalPrice: 0,
            supplierId: '',
            submitBy: '',
            purchaseOrderDetail: [{
                quantity: 0,
                price: 0,
                productId: ''
            }]
        })
        setDetailForm([{
            quantity: 0,
            price: 0,
            productId: ""
        }])
        let supplier: DropdownModel = {
            label: '',
            value: 0
        };
        let product: DropdownModel[] = [{
            label: '',
            value: ''
        }]
        setSelectedSupplier(supplier);
        setSelectedProduct(product)
    }

    useEffect(() => {
        fetchData();
        setUsername(cookies.username);
    }, [])

    return (
        <div>
            <LoadingPage loading={isSubmitting}/>
            <BreadCrumbs href="/purchaseOrder" isChanged={isChanged} text="Kembali ke Daftar Purchase Order"/>
            <h3 className="mb-4">Buat Purchase Order</h3>
            <hr />
            <form onSubmit={onSubmit}>
                <fieldset disabled={isSubmitting}>
                    <div className="mb-3">
                        <label className="fw-bold mb-2" htmlFor="country">Supplier</label>
                        <Select
                            options={supplierOptions}
                            value={selectedSupplier}
                            onChange={onSupplierChange}
                        />
                        <span className="text-danger small">{formState.supplierId.error}</span>
                    </div>
                    {detailForm.map((element, index) => {
                        return (
                            <div className="row" key={index}>
                                <div className="col">
                                    <label className="fw-bold mb-2" htmlFor="country">Barang</label>
                                    <Select
                                        options={productOptions}
                                        value={selectedProduct[index]}
                                        onChange={(selected) => onProductChange(selected, index)}
                                    />
                                    <span className="text-danger small">{formState.purchaseOrderDetail[index]?.productId.error}</span>
                                </div>
                                <div className="col">
                                    <label className="fw-bold mb-2" htmlFor="kuantitas">Kuantitas</label>
                                    <input id="kuantitas" className='form-control'
                                        value={element.quantity} onChange={onArrayChange("quantity", index)}
                                    ></input>
                                    <span className="text-danger small">{formState.purchaseOrderDetail[index]?.quantity.error}</span>
                                </div>
                                <div className="col">
                                    <label className="fw-bold mb-2" htmlFor="harga">Harga</label>
                                    <input id="harga" className='form-control'
                                        value={element.price} onChange={onArrayChange("price", index)}
                                    ></input>
                                    <span className="text-danger small">{formState.purchaseOrderDetail[index]?.price.error}</span>
                                </div>
                                {
                                    index ?
                                        <div className="col mb-2">
                                            <button type="button" className="btn btn-secondary mt-4" onClick={() => removeFormFields(index)}>
                                                <FontAwesomeIcon icon={faMinus}></FontAwesomeIcon>
                                                <span className="ms-2"> Hapus barang</span>
                                            </button>
                                        </div>
                                        :
                                        null
                                }
                            </div>
                        )
                    })}
                    <div className="mb-3 mt-3">
                        <button type="button" className="btn btn-secondary" onClick={() => addInputFields()}>
                            <FontAwesomeIcon icon={faPlus}></FontAwesomeIcon>
                            <span className="ms-2"> Tambah barang</span>
                        </button>
                    </div>
                    <div className='d-flex'>
                        <p className='ms-auto my-auto'>Total Harga: <PriceFormat value={form.totalPrice}/></p>
                        <div className="col-1"></div>
                        <button className='btn btn-primary my-auto' type="submit">Kirim</button>
                    </div>
                </fieldset>
            </form>
        </div>
    )
}

export default function PurchaseOrderPage() {
    return (
        <Authorize onlyLoggedIn={true}>
            <Layout title='Create Purchase Order'>
                <CreatePurchaseOrder></CreatePurchaseOrder>
            </Layout>
        </Authorize>
    )
};